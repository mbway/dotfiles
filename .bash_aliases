
# see ~/bin/portable for longer scripts


# suppress the PID from being printed, which happens with simply terminator &
alias terminator="(terminator &> /dev/null &) > /dev/null"
alias term="terminator"
# need to allow access to all files in flatseal
alias gg="flatpak run com.github.git_cola.git-cola --repo . > /dev/null 2>&1 &"
alias lk="qdbus org.freedesktop.ScreenSaver /ScreenSaver Lock"
alias open="xdg-open"
alias pip="pip_editable"

# from https://github.com/casey/just#shell-alias
alias j="just"
if command -v _completion_loader > /dev/null; then
    _completion_loader just
    # _just is the name of the autocompleter for just (loaded by _completion_loader above)
    complete -F _just -o bashdefault -o default j
fi


private-session() { command private-session; history -d "$(history 1)"; }


if command -v vscodium &> /dev/null; then
    alias code="vscodium"
elif command -v codium &> /dev/null; then
    alias code="codium"
fi

alias vim=nvim
if command -v neovide &> /dev/null; then
    gvim() {
        (neovide --grid 125x50 "$@") &
    }
    # by default, neovide opens in tabs. -O opens in vertical splits instead
    alias gvimdiff="neovide -- -d -O"
elif command -v nvim-qt &> /dev/null; then
    alias gvim="nvim-qt"
fi


yay () {
    if [[ $(which python) =~ "/usr/" ]]; then
        # 'command' gets the actual command from PATH, not this function
        command yay "$@"
    else
        echo "should not run yay with a python virtual environment active"
    fi
}

# always start emacs in the background
emacs () {
  /usr/bin/emacs "$@" &
}

# `fm` alias is provided by a script


alias dlp="yt-dlp --no-mtime"
dlp-resolution () {
    # eg dlp-resolution 720 <URL> for 720p
    yt-dlp -f "best[height<=$1]" "$2"
}


# avoid overwriting destination without asking
alias cp='cp -i'
alias mv='mv -i'
alias l='ls --color=auto -h'
alias ls='ls --color=auto -h'
alias la='ls --color=auto -la'
alias ..='cd ..'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias pacrm='sudo pacman -Rns'
