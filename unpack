#!/usr/bin/env python3
"""
A simple script for setting up symlinks from the current directory to $HOME.
Will not touch files which already exist. Safe to run as many times as you like.
"""

import os
import sys
import shutil
import argparse
import filecmp
from pathlib import Path

# TODO: handle .config/nvim/init.vim (is local symlink)
# TODO: .ctags can't have comments when using exuberant ctags. have .ctags.readme which has comments and generate .ctags from it by stripping comments
# TODO(matt): more sophisticated ignores (e.g. match glob pattern) so that temporary files like .swp files can be ignored
# TODO(matt): command to adopt existing files and automatically move them to dotfiles and create a symlink

GREEN = "\033[92m"
BLUE = "\033[94m"
RED = "\033[91m"
END_COLOR = "\033[0m"

# TODO: could use hardlinks instead


def colored(msg: str, color: str) -> str:
    return color + msg + END_COLOR


def list_rec(root: Path, ignore_paths: set[Path]) -> list[Path]:
    """all paths are absolute"""
    if root in ignore_paths:
        return []
    elif os.path.isfile(root):
        return [root]

    paths: list[Path] = []
    for i in root.iterdir():
        if i in ignore_paths:
            continue

        if i.is_dir():
            paths += list_rec(i, ignore_paths)
        else:
            paths.append(i)
    return paths


def evaluate_destination(abs_src: Path, file_dest: Path, args: argparse.Namespace) -> bool:
    """returns whether the file should be linked/copied"""
    indent = " " * 4
    should_copy = bool(abs_src in args.copy_file or args.copy_all)

    if not os.path.lexists(file_dest):  # if a symlink: don't follow
        # destination doesn't exist
        return True

    elif file_dest.is_symlink():
        if should_copy:
            print(indent + colored("File marked to be a copy but exists and is a symlink", RED))
        else:
            try:
                abs_dest = file_dest.resolve()
            except:
                print(indent + colored("Destination symlink is a symlink loop!", RED))
                return False

            if abs_dest == abs_src:
                print(indent + colored("Already a symlink with the correct target", GREEN))
            else:
                print(
                    indent
                    + colored(
                        f'A symlink pointing at another location: "{file_dest.resolve()}"',
                        RED,
                    )
                )
                if args.overwrite_symlinks:
                    print(indent + colored(f"Removing because --overwrite-symlinks", RED))
                    file_dest.unlink()
                    return True
                else:
                    print(indent + colored(f"Pass --overwrite-symlinks to overwrite", RED))

    elif file_dest.is_file():
        if should_copy:
            if filecmp.cmp(abs_src, file_dest):
                print(indent + colored("Already a _copy_ with the correct content", GREEN))
            else:
                print(indent + colored("A copy with different content", RED))
        else:
            print(indent + colored("Exists and is a file", RED))

    else:
        print(indent + colored("Something else (fix manually)", RED))

    return False


def perform_unpack_unconditionally(abs_src: Path, file_dest: Path, args: argparse.Namespace) -> None:
    assert not file_dest.exists()

    if not file_dest.parent.is_dir():
        print(colored("    Parent did not exist creating", BLUE))
        if args.dry_run:
            print(colored("    --dry-run: no action taken", BLUE))
        else:
            file_dest.parent.mkdir(parents=True, exist_ok=False)

    if abs_src in args.copy_file or args.copy_all:
        print(colored("    Did not exist, copying...", BLUE))
        if args.dry_run:
            print(colored("    --dry-run: no action taken", BLUE))
        else:
            shutil.copyfile(abs_src, file_dest)
    else:
        print(colored("    Did not exist, linking...", BLUE))
        if args.dry_run:
            print(colored("    --dry-run: no action taken", BLUE))
        else:
            file_dest.symlink_to(abs_src)


def unpack(source_dir: Path, files: list[Path], destination_dir: Path, args: argparse.Namespace) -> None:
    files = [f.relative_to(source_dir) for f in files]
    for f in files:
        file_dest = destination_dir / f
        abs_src = (source_dir / f).resolve(strict=True)  # if f is itself a symlink, resolve to avoid indirection
        print(str(file_dest))

        should_link_or_copy = evaluate_destination(abs_src, file_dest, args)
        if should_link_or_copy:
            perform_unpack_unconditionally(abs_src, file_dest, args)


def load_unpack_rc(rc_file: Path) -> list[str]:
    if rc_file.exists():
        with rc_file.open(mode="r") as f:
            lines = [l.strip().split(" ") for l in f.readlines() if not l.lstrip().startswith("#")]
            return [part for l in lines for part in l]
    return []


def main(rc_file: Path | None = None):
    parser = argparse.ArgumentParser(description="Unpack (symlink) dotfiles to the home directory")
    parser.add_argument("--source", help="where to unpack from (defaults to CWD)")
    parser.add_argument("--destination", help="where to unpack to (defaults to $HOME)")
    parser.add_argument("-a", "--all", help="unpack everything", action="store_true")
    parser.add_argument("--dry-run", help="don't actually unpack anything", action="store_true")
    parser.add_argument(
        "--overwrite-symlinks",
        help="if the target already exists and is a symlink: overwrite it",
        action="store_true",
    )
    parser.add_argument(
        "-i",
        "--ignore",
        help="exclude a path (relative to --source) from being searched/unpacked. Also Reads ignore paths from source/.unpack_ignore",
        type=str,
        action="append",
    )
    parser.add_argument(
        "-c",
        "--copy-file",
        help="rather than symlinking: copy the specified file",
        type=str,
        action="append",
    )
    parser.add_argument(
        "--copy-all",
        help="rather than symlinking: copy all files",
        action="store_true",
    )
    parser.add_argument(
        "search_paths",
        metavar="SEARCH_PATH",
        type=str,
        nargs="*",
        help="the directories to unpack (relative to --source) (none given => --all)",
    )

    if rc_file is None:
        rc_file = Path(".unpackrc")

    extra_args = load_unpack_rc(rc_file)
    args = parser.parse_args(extra_args + sys.argv[1:])

    args.source = Path(args.source).resolve(strict=True) if args.source is not None else Path.cwd()
    args.destination = Path(args.destination).resolve(strict=True) if args.destination is not None else Path.home()

    if args.all or not args.search_paths:
        args.search_paths = [args.source]
    else:
        args.search_paths = [args.source / p for p in args.search_paths]

    args.copy_file = (
        set() if args.copy_file is None else {(args.source / p).resolve(strict=True) for p in args.copy_file}
    )

    ignore_paths: set[Path] = (
        set() if args.ignore is None else {(args.source / p).resolve(strict=False) for p in args.ignore}
    )
    args.ignore = ignore_paths

    files = [list_rec(path, ignore_paths) for path in args.search_paths]
    files = sorted([f for l in files for f in l])  # flatten and sort

    unpack(args.source, files, args.destination, args)


if __name__ == "__main__":
    main()
