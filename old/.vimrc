if !has("nvim")
    " Use Vim settings, rather than Vi settings
    " This must be first, because it changes other options as a side effect.
    set nocompatible
endif

" type gf to go to the file under the cursor, then <C-i>/<C-o> to navigate forward and back

source ~/.vim/vimrc/plugins.vim
source ~/.vim/vimrc/basics.vim
source ~/.vim/vimrc/functions.vim
source ~/.vim/vimrc/version_specific.vim
source ~/.vim/vimrc/keybindings.vim
source ~/.vim/vimrc/language_settings.vim

"source ~/.vim/vimrc/disabled.vim
"source ~/.vim/vimrc/unicode.vim


" ### Debugging ###
" - run with `gvim -u NONE` to prevent loading the vimrc at startup
" - run `:checkhealth` if some plugins aren't working (like deoplete)
" - to get a description of a variable use `:help varname`. Variables with spaces become hyphenated
"   eg. `filetype plugin on` -> `:help filetype-plugin`
" - to determine where a variable was last set `:verbose set varname?`
" - view current settings using `:set varname?`
" - `:so %` while editing this file to re-load
" - `:map` to view the current keybindings (or `:nmap`, `:imap`, `:vmap`)
" - `:verbose map` to view the current keybindings and where last defined! (or nmap, imap etc)
" - start vim with `vim -V9vim.log` to write to `vim.log` with verbosity 9. Useful for debugging startup issues
" - `:messages` will show messages

" ### Bootstrapping ###
" move the .vimrc into place, not on Windows: _vimrc instead
" visit https://github.com/junegunn/vim-plug and follow the installation instructions
" execute :PlugInstall
"
" Set the keyboard repeat delay and rate on the system to approximately:
" Delay 210ms
" Rate 30 repeats/sec or 33ms delay interval between
" for Ubuntu/Gnome
" /usr/bin/gsettings set org.gnome.desktop.peripherals.keyboard delay 210
" /usr/bin/gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 33
"
"
" ## Windows ##
"   different format for fonts:
"       set guifont=Source_Code_Pro:h14
"   need more columns to make the window a reasonable width
"       set columns=120


" ### Tips ###
" - can use Ctrl+[ as escape
" - Make tip. Useful when using makefile with pandoc since errors cause the Makefile to be opened,
"   not the markdown. Use `:make!` to prevent jumping to the location of any errors.
