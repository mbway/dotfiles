" Unicode
" good reference: https://github.com/derekelkins/agda-vim/blob/master/agda-utf8.vim
if 1

let maplocalleader="%"

" Misc
imap <buffer> <LocalLeader>:: ∷

" Maths
imap <buffer> <LocalLeader>partial ∂
imap <buffer> <LocalLeader>to →
imap <buffer> <LocalLeader>r →
imap <buffer> <LocalLeader>in ∈
imap <buffer> <LocalLeader>== ≡
imap <buffer> <LocalLeader>equiv ≡
imap <buffer> <LocalLeader>approx ≈
imap <buffer> <LocalLeader>cup ∪
imap <buffer> <LocalLeader>cap ∩
imap <buffer> <LocalLeader>neq ≠

" logic
imap <buffer> <LocalLeader>forall ∀
imap <buffer> <LocalLeader>exists ∃
imap <buffer> <LocalLeader>and ∧
imap <buffer> <LocalLeader>or ∨
imap <buffer> <LocalLeader>neg ¬
imap <buffer> <LocalLeader>implies ⇒
imap <buffer> <LocalLeader>=> ⇒
imap <buffer> <LocalLeader>always □
imap <buffer> <LocalLeader>eventually ◇
imap <buffer> <LocalLeader>next ◯
imap <buffer> <LocalLeader>always □


" Lower Case Greek
imap <buffer> <LocalLeader>alpha α
imap <buffer> <LocalLeader>beta β
imap <buffer> <LocalLeader>gamma γ
imap <buffer> <LocalLeader>delta δ
imap <buffer> <LocalLeader>epsilon ε
imap <buffer> <LocalLeader>zeta ζ
imap <buffer> <LocalLeader>eta η
imap <buffer> <LocalLeader>theta θ
imap <buffer> <LocalLeader>iota ι
imap <buffer> <LocalLeader>kappa κ
imap <buffer> <LocalLeader>lambda λ
imap <buffer> <LocalLeader>mu μ
imap <buffer> <LocalLeader>nu ν
imap <buffer> <LocalLeader>xi ξ
imap <buffer> <LocalLeader>omicron ο
imap <buffer> <LocalLeader>pi π
imap <buffer> <LocalLeader>rho ρ
imap <buffer> <LocalLeader>sigma σ
imap <buffer> <LocalLeader>varsigma ς
imap <buffer> <LocalLeader>tau τ
imap <buffer> <LocalLeader>upsilon υ
imap <buffer> <LocalLeader>phi φ
imap <buffer> <LocalLeader>chi χ
imap <buffer> <LocalLeader>psi ψ
imap <buffer> <LocalLeader>omega ω

" Upper Case Greek
imap <buffer> <LocalLeader>Alpha Α
imap <buffer> <LocalLeader>Beta Β
imap <buffer> <LocalLeader>Gamma Γ
imap <buffer> <LocalLeader>Delta Δ
imap <buffer> <LocalLeader>Epsilon Ε
imap <buffer> <LocalLeader>Zeta Ζ
imap <buffer> <LocalLeader>Eta Η
imap <buffer> <LocalLeader>Theta Θ
imap <buffer> <LocalLeader>Iota Ι
imap <buffer> <LocalLeader>Kappa Κ
imap <buffer> <LocalLeader>Lambda Λ
imap <buffer> <LocalLeader>Mu Μ
imap <buffer> <LocalLeader>Nu Ν
imap <buffer> <LocalLeader>Xi Ξ
imap <buffer> <LocalLeader>Omicron Ο
imap <buffer> <LocalLeader>Pi Π
imap <buffer> <LocalLeader>Rho Ρ
imap <buffer> <LocalLeader>Sigma Σ
imap <buffer> <LocalLeader>Tau Τ
imap <buffer> <LocalLeader>Upsilon Υ
imap <buffer> <LocalLeader>Phi Φ
imap <buffer> <LocalLeader>Chi Χ
imap <buffer> <LocalLeader>Psi Ψ
imap <buffer> <LocalLeader>Omega Ω

" Subscript (not all letters possible)
" ₐbcdₑfgₕᵢⱼₖₗₘₙₒₚqᵣₛₜᵤᵥwₓyz
imap <buffer> <LocalLeader>_a ₐ
imap <buffer> <LocalLeader>_e ₑ
imap <buffer> <LocalLeader>_h ₕ
imap <buffer> <LocalLeader>_i ᵢ
imap <buffer> <LocalLeader>_j ⱼ
imap <buffer> <LocalLeader>_k ₖ
imap <buffer> <LocalLeader>_l ₗ
imap <buffer> <LocalLeader>_m ₘ
imap <buffer> <LocalLeader>_n ₙ
imap <buffer> <LocalLeader>_o ₒ
imap <buffer> <LocalLeader>_p ₚ
imap <buffer> <LocalLeader>_r ᵣ
imap <buffer> <LocalLeader>_s ₛ
imap <buffer> <LocalLeader>_t ₜ
imap <buffer> <LocalLeader>_u ᵤ
imap <buffer> <LocalLeader>_v ᵥ
imap <buffer> <LocalLeader>_x ₓ

imap <buffer> <LocalLeader>_0 ₀
imap <buffer> <LocalLeader>_1 ₁
imap <buffer> <LocalLeader>_2 ₂
imap <buffer> <LocalLeader>_3 ₃
imap <buffer> <LocalLeader>_4 ₄
imap <buffer> <LocalLeader>_5 ₅
imap <buffer> <LocalLeader>_6 ₆
imap <buffer> <LocalLeader>_7 ₇
imap <buffer> <LocalLeader>_8 ₈
imap <buffer> <LocalLeader>_9 ₉

imap <buffer> <LocalLeader>_+ ₊
imap <buffer> <LocalLeader>_- ₋

" Superscript (all except q)
imap <buffer> <LocalLeader>^a ᵃ
imap <buffer> <LocalLeader>^b ᵇ
imap <buffer> <LocalLeader>^c ᶜ
imap <buffer> <LocalLeader>^d ᵈ
imap <buffer> <LocalLeader>^e ᵉ
imap <buffer> <LocalLeader>^f ᶠ
imap <buffer> <LocalLeader>^g ᵍ
imap <buffer> <LocalLeader>^h ʰ
imap <buffer> <LocalLeader>^i ⁱ
imap <buffer> <LocalLeader>^j ʲ
imap <buffer> <LocalLeader>^k ᵏ
imap <buffer> <LocalLeader>^l ˡ
imap <buffer> <LocalLeader>^m ᵐ
imap <buffer> <LocalLeader>^n ⁿ
imap <buffer> <LocalLeader>^o ᵒ
imap <buffer> <LocalLeader>^p ᵖ
" no q
imap <buffer> <LocalLeader>^r ʳ
imap <buffer> <LocalLeader>^s ˢ
imap <buffer> <LocalLeader>^t ᵗ
imap <buffer> <LocalLeader>^u ᵘ
imap <buffer> <LocalLeader>^v ᵛ
imap <buffer> <LocalLeader>^w ʷ
imap <buffer> <LocalLeader>^x ˣ
imap <buffer> <LocalLeader>^y ʸ
imap <buffer> <LocalLeader>^z ᶻ

imap <buffer> <LocalLeader>^0 ⁰
imap <buffer> <LocalLeader>^1 ¹
imap <buffer> <LocalLeader>^2 ²
imap <buffer> <LocalLeader>^3 ³
imap <buffer> <LocalLeader>^4 ⁴
imap <buffer> <LocalLeader>^5 ⁵
imap <buffer> <LocalLeader>^6 ⁶
imap <buffer> <LocalLeader>^7 ⁷
imap <buffer> <LocalLeader>^8 ⁸
imap <buffer> <LocalLeader>^9 ⁹

imap <buffer> <LocalLeader>^+ ⁺
imap <buffer> <LocalLeader>^- ⁻

endif

