" General Functionality Preferences

" syntax highlighting
syntax enable
syntax on
filetype on
filetype plugin on
filetype indent on


" ### Light Color Schemes ###
"set background=light
    "colorscheme github
    "colorscheme oceanlight
    "colorscheme solarized

" ### Dark Color Schemes ###
set background=dark
    "colorscheme molokai
    colorscheme Monokai
    "colorscheme monokain
    "colorscheme mopkai
    "colorscheme desert
    "colorscheme mint
    "colorscheme solarized

" ### Fonts ###
" I am handling fonts in a version-agnostic way and actually setting the font later
"
" - For reference, visual studio code font priorities:
"   Monaco, Menlo, Consolas, Droid Sans Mono, Inconsolata, Courier New, monospace
" - note: can use Ctrl+r =&guifont <ENTER> to write the current value of guifont to the buffer
"   (eg after setting with Edit>Select Font)
" - https://github.com/ProgrammingFonts/ProgrammingFonts
" - one day this font might be supported: https://github.com/tonsky/FiraCode

    let g:fontsize=12
    let g:fontweight=''   " '' for regular, 'l' for light and 'b' for bold

" ### Smooth Fonts ###
    "let g:fontfamily="Anonymous Pro"
    "let g:fontfamily="DejaVu Sans Mono"
    "let g:fontfamily="Hack"
    "let g:fontfamily="Inconsolata"
    "let g:fontfamily="Liberation Mono"
    "let g:fontfamily="Menlo"
    "let g:fontfamily="Monaco"
    "let g:fontfamily="Noto Mono"
    let g:fontfamily="Source Code Pro"
    "let g:fontfamily="Droid Sans Mono"
    "let g:fontfamily="Ubuntu Mono"

" ### Blocky fonts ###
    "let g:fontfamily="ProggyCleanTT"

" ### Compatibility Fonts ###
    "let g:fontfamily="DejaVu Sans Mono"
    "let g:fontfamily="Monospace"


let g:custom_abbreviations_enabled=1

set encoding=utf-8

" use the system clipboard (* register) (select to copy, middle mouse to paste)
set clipboard=unnamed
" reason why you might not want to use +: operations such as deleting copies content to the clipboard
" in Vim, and the following is common: copy, enter Vim, delete something then paste.
"set clipboard=unnamedplus " use the system clipboard (+ register) (Ctrl+C, Ctrl+v in most applications)

set spelllang=en_gb
" add good spellings with zg and wrong spellings with zw
set spellfile=$HOME/.myDictionary.utf-8.add
set spell

set mouse=a  " enable mouse support

"set autochdir " set the editor CWD to that of the open file. Not always the best for searching

" text formatting / wrapping (gq)
set textwidth=120
set colorcolumn=120 " colored vertical line to indicate when lines are too long
" set formatoptions=q " no auto-wrap, only using gq (see :help fo-table)
" filetype plugins overwrite simply setting the formatoptions variable
autocmd BufNewFile,BufRead,BufEnter * setlocal formatoptions=q
set wrap " enable line wrapping
set linebreak " break only at characters in 'breakat' (`:set breakat?` to view characters)


set scrolloff=3 " always show at least X lines above or below the cursor
set sidescrolloff=5

set undolevels=2000 " more undo history
set history=1000 " command line history

set autoread " reload files automatically if externally edited

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" configure tabs
" number of columns = 1 tab
set tabstop=4     " how tabs are displayed. Might need to be changed to view other's
                  " code properly (see :help tabstop)
set shiftwidth=4  " move this many columns when << or >> (should be equal to tabstop)
set softtabstop=4 " the number of columns to use for an indentation (should be equal to tabstop)
                  " makes spaces 'feel' like they are tab characters
set expandtab     " expand tab character to spaces

set number      " Show line numbers
"set cursorline " highlight the current line. In some themes, harder to read
set showmatch   " briefly highlight a matching bracket when typing a new one

set autoindent  " copy indentation from previous line
" set smartindent " trying to be smart often ends up being more of a pain

set ruler   " show the cursor position all the time

set showcmd " show commands as they are typed out

" show possible auto-completions for commands / paths when tab pressed when typing in the command line
set wildmenu

set laststatus=2  " always show status bar (default: open when split)

" non-printable characters view tab characters and trailing spaces.
" enable with `:set list`, disable with `:set nolist`
set listchars=tab:»\ ,trail:·,eol:$

" folding
set foldmethod=syntax
set foldenable
set foldlevelstart=999
set foldlevel=999


" temporary files.
if !isdirectory($HOME . "/.cache/vim")
    call mkdir($HOME . "/.cache/vim", "p", 0700)
    call mkdir($HOME . "/.cache/vim/backup", "p", 0700)
    call mkdir($HOME . "/.cache/vim/swap", "p", 0700)
    call mkdir($HOME . "/.cache/vim/undo", "p", 0700)
endif
" note // means that directory information will be encoded into the filename
" a backup is a copy from before editing. e.g. myfile.txt~
set backupdir=~/.cache/vim/backup//
" sets swap file directory. A swap file contains unsaved changes. is e.g. myfile.txt.swp
set directory=~/.cache/vim/swap//
" an undo file contains the undo tree. e.g. myfile.txt.un~
set undodir=~/.cache/vim/undo//



" ### Search ###
" note on regex:
" by default: no special characters have special meaning, they are interpreted
" literally. To enable the special meaning, escape with backslashes. To get the
" equivalent of grep -E (extended regex) put \v at the start (standing for 'very
" magic')
set incsearch  " start searching before pressing enter
set nohlsearch
" ignore case unless \C is used or if an upper case letter is entered in a search: switch to case sensitive
" there seems to be a bug or something which means that to set smartcase, you have to set ignorecase first...
set ignorecase
set smartcase

" visual shifting (does not exit visual mode)
vnoremap < <gv
vnoremap > >gv

" count underscore as a word boundary
"set iskeyword-=_


" ### Make ###
" run Makefile. If there isn't one: check parent directory (not recursive)
" makeprg defines what :make does
" set makeprg='make'
let &makeprg = 'if [ -f Makefile ]; then make; else make -C ..; fi'

" open quickfix after :make
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" ### Tags ###
" use universal-ctags which supports many languages: https://ctags.io/
" better than exuberant-ctags (is a drop-in replacement)
" to generate a tags file (eg put in Makefile): ctags -R .

" look in the editor CWD (and all parents up to /) and in the current file's directory (./)
" and parents up to root for a tags file.
" syntax is `<path>;<stop-directory>,<path>;<stop-directory>`. By leaving stop-directory blank: recurse until root.
set tags=tags;,./tags;

" browser to use when opening a URL with gx
let g:netrw_browsex_viewer='firefox'

