" ### Disabled Options ###
" kept in case I want to revisit them
if 0


" relative line numbering
" use set relativenumber! to go back to absolute numbering
set relativenumber

" F1 to toggle relative and absolute numbering
function! NumberToggle()
    if(&relativenumber == 1)
        set relativenumber!
    else
        set relativenumber
    endif
endfunc

nnoremap <F1> :call NumberToggle()<cr>


" alternative to escape
" I don't like this option because of the enforced delay when typing j
inoremap jj <ESC>
imap jk <esc>
" map shift+enter to escape
imap <S-CR> <Esc>

" Disable arrow keys because that's what the cool kids do
map <Up>     <Nop>
map <Down>   <Nop>
map <Left>   <Nop>
map <Right>  <Nop>

imap <up>    <nop>
imap <down>  <nop>
imap <left>  <nop>
imap <right> <nop>

" move through very long wrapped lines easier
nnoremap <Down> gj
nnoremap <Up> gk
" consider only having these active when editing lots of text (eg tex documents)
nnoremap j gj
nnoremap k gk

" copy the entire file to the system clipboard
nnoremap <leader><C-y> :%y+<CR>
" select the whole line, but exclude the newline character (g_)
nnoremap <leader>V 0vg_

" to and from hex using xxd
nnoremap <leader>tx :%!xxd<CR>
nnoremap <leader>fx :%!xxd -r<CR>

" readline / Emacs home and end keys
" have to disable to use C-a for incrementing numbers
nnoremap <C-e> $
nnoremap <C-a> ^

endif
