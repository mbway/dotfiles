" ### Plugins ###
" uses vim-plug (install instructions at link):
" https://github.com/junegunn/vim-plug
"
" PlugInstall [name ...] [#threads] Install plugins
" PlugUpdate [name ...] [#threads]  Install or update plugins
" PlugClean[!]                      Remove unused directories (bang version will clean without prompt)
" PlugUpgrade                       Upgrade vim-plug itself
" PlugStatus                        Check the status of plugins
" PlugDiff                          Examine changes from the previous update and the pending changes
" PlugSnapshot[!] [output path]     Generate script for restoring the current snapshot of the plugins


" ### Plugins To Get ###
" https://github.com/mg979/vim-visual-multi   (multi-cursor)
" nerdcommenter
" easy motion: places a-z to choose
" quick-scope: highlights good characters to f/t jump to https://github.com/unblevable/quick-scope
" neco-ghc Haskell completion using GHC
" ALE


" ### External Requirements ###
"   sudo apt install silversearcher-ag
" for guten tags
"   sudo snap install universal-ctags
" for autoswap
"   sudo apt install wmctrl
" for prettier
"   curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
"   echo 'deb https://dl.yarnpkg.com/debian/ stable main' | sudo tee /etc/apt/sources.list.d/yarn.list
"   sudo apt update
"   sudo apt install yarn nodejs

let g:use_language_servers=1


call plug#begin('~/.vim/plugged')

if has("nvim")
    Plug 'equalsraf/neovim-gui-shim' " shim shouldn't be needed but just in case (for nvim-qt)
    Plug 'dstein64/nvim-scrollview', { 'branch': 'main' }
endif


" ## Personalisation ##
" good fonts: https://github.com/ProgrammingFonts/ProgrammingFonts
Plug 'flazz/vim-colorschemes'
Plug 'xolox/vim-misc' | Plug 'xolox/vim-colorscheme-switcher'
Plug 'powerline/fonts', {'do' : './install.sh' } | " patched fonts for fancy status bars
    Plug 'vim-airline/vim-airline' | " a better status bar
        Plug 'vim-airline/vim-airline-themes'


" ## Small Added Functionality ##
Plug 'godlygeek/tabular' " alignment
Plug 'danro/rename.vim' " adds :rename filename
Plug 'bronson/vim-visual-star-search' " search for visual selected text with * and # (useful for Unicode text)
" adds `s` object for surrounding.
" e.g. cs"' to change surrounding " to '
" e.g. with text visually selected: S) to surround selection with brackets
" e.g. with text visually selected: S<em> to surround selection with <em> tags (automatically closes)
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat' " enables repeating vim-surround commands with .
" enables saving with sudo while neovim has an issue (see plugin github). Used for w!! keybinding.
Plug 'lambdalisue/suda.vim'
" when opening a file which is already open: switch to that window instead. (requires wmctrl)
" note: doesn't work if the buffer isn't focused because it uses the window title to determine what to switch to
"Plug 'gioele/vim-autoswap'  " use my fork until it is merged upstream
Plug 'mbway/vim-autoswap'

" Fixes an issue encountered with netrw (a built in vim plugin) which provides the default 'gx' implementation
" to go to a URL. This plugin provides advantages over the built in gx in addition to working properly.
" (the bug I was encountering was that wget would be used to download urls then firefox would open. I have
" found other experiencing the same issue but no good fixes)
" This plugin has a slight issue where absolute local paths are not handled correctly but this is a rare
" use case anyway.
" the issue could be related to this: https://github.com/vim/vim/pull/7188
let g:netrw_nogx = 1
" cfile matches the netrw default and handles markdown urls enclosed in <> better. Despite what the plugin
" documentation claims, it also seems to handle urls with query parameters in them.
let g:xdg_open_match = '<cfile>'
Plug 'arp242/xdg_open.vim'


" ## Passive Functionality ##
Plug 'ntpeters/vim-better-whitespace' " highlights trailing whitespace
"Plug 'kien/rainbow_parentheses.vim' ", { 'on':  'RainbowParenthesesToggle' } color nested parentheses
Plug 'kshenoy/vim-signature' " show marks in the sidebar
Plug 'airblade/vim-gitgutter' " show status of each line (added, deleted etc) in the gutter
"Plug 'scrooloose/syntastic',   { 'on':  'SyntasticToggleMode' } " show syntax errors in gutter
"Plug 'octol/vim-cpp-enhanced-highlight'


" ## Tools ##
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'majutsushi/tagbar' , { 'on':  'TagbarToggle' }
" <c-j> and <c-k> to navigate results. <c-f> and <c-b> to cycle search modes.
"Plug 'ctrlpvim/ctrlp.vim' " fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' } " fuzzy finder (better than ctrlp). Command line tool and basic wrapper
Plug 'junegunn/fzf.vim' " adds better wrapper to fzf functionality (:Files, :Buffers, :Lines etc)
" prettifier. Requires nodejs and yarn to be installed
" Plug 'prettier/vim-prettier', {
"   \ 'do': 'yarn install',
"   \ 'for': ['javascript', 'typescript', 'css', 'json', 'yaml', 'html'] }

" ## Language Support ##
" lightweight LaTeX support for autocompletion, compilation and viewing see :help latex-box
Plug 'LaTeX-Box-Team/LaTeX-Box', { 'for': 'tex' }
Plug 'neovimhaskell/haskell-vim', { 'for': 'haskell' } " better syntax highlighting and other stuff
Plug 'xolox/vim-misc' | Plug 'xolox/vim-lua-ftplugin', { 'for': 'lua' } " better lua support
Plug 'kchmck/vim-coffee-script', { 'for': 'coffee' }
Plug 'habamax/vim-godot'
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
" vim-pandoc must also be installed to set the filetype correctly (see github page)
Plug 'vim-pandoc/vim-pandoc', { 'for': 'markdown' } | " pandoc-markdown syntax
    Plug 'vim-pandoc/vim-pandoc-syntax', { 'for': 'markdown' }
" Python
Plug 'vim-python/python-syntax', { 'for': 'python' }
"Plug 'sirtaj/vim-openscad' " openscad (CAD file format) syntax highlighting
Plug 'cespare/vim-toml', { 'branch': 'main' }
" justfile
Plug 'NoahTheDuke/vim-just', { 'for': 'just' }



let g:scrollview_column = 1

" ## Auto completion ##
if g:use_language_servers
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
endif

call plug#end()  " Add plugins to &runtimepath


" python-syntax
let g:python_highlight_all = 1

" Flake8
let g:flake8_show_in_gutter=1
let g:flake8_quickfix_height=10
let g:flake8_show_in_file=1

" LaTeX box plugin settings
"   latexmk_options will cause the output to be placed in build/, build_dir
"   makes sure the plugin can find the output log to display errors correctly
let g:LatexBox_latexmk_options='-outdir=build'
let g:LatexBox_build_dir='build'


" use the_silver_searcher (fast c implementation of Ack) if installed
" apt install silversearcher-ag
if executable('ag')
    " Use ag over grep
    set grepprg=ag\ --nogroup\ --nocolor
endif

" pandoc markdown plugin
" from: https://github.com/vim-pandoc/vim-pandoc-syntax
augroup pandoc_syntax
    autocmd! BufNewFile,BufFilePRe,BufRead *.md set filetype=markdown.pandoc
augroup END
let g:pandoc#syntax#conceal#use=0
let g:pandoc#keyboard#display_motions=0 " whether to map j to gj and k to gk etc

" NerdTree
" the default binding of 'm' conflicts with vim-signature's binding, causing the popup menu to be very slow
" tips: M to bring up menu to do things like create files or open the directory in the file manager
let g:NERDTreeMapMenu='M'

" airline
"let g:airline#extensions#tabline#enabled = 1
"let g:airline_extensions = 0
" depends on the font used for the editor, see supported fonts here: https://github.com/powerline/fonts
let g:airline_powerline_fonts = 0
let g:airline_theme='minimalist'
let g:airline#extensions#tagbar#enabled = 1
" help tagbar-statusline for flag meanings
let g:airline#extensions#tagbar#flags = 'f'
" since I always have spelling enabled, I don't need to see it in the status line
let g:airline_detect_spell=0

" colorscheme-switcher
let g:colorscheme_switcher_define_mappings = 0 " don't map to F8

" gitgutter
let g:gitgutter_enabled = 1
let g:gitgutter_map_keys = 0  " keybindings for staging
" 500 is the default
let g:gitgutter_max_signs = 1000

" vim-cpp-enhanced-highlight
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1

" prettier
" don't format on saving
let g:prettier#autoformat = 0
let g:prettier#config#print_width = 120
"nunmap <leader>p


" Coc
" install language servers:
" sudo apt-get install clangd

if g:use_language_servers

    " https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions#implemented-coc-extensions
    let g:coc_global_extensions = [
        \ 'coc-jedi',
        \ 'coc-css', 'coc-html', 'coc-json',
        \ 'coc-cmake', 'coc-clangd',
        \ 'coc-rust-analyzer'
    \ ]

    " see github for suggested configuration: https://github.com/neoclide/coc.nvim

    set nobackup
    set nowritebackup


    " time after typing stops that the swap file is written
    " linting kicks in after swap is written so the default is too long to be responsive
    set updatetime=300

    nmap <silent> <space> <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)
    nmap <F2> <Plug>(coc-rename)

    nmap <leader>oi :call CocAction('runCommand', 'editor.action.organizeImport')<CR>


    " Use K to show documentation in preview window.
    nnoremap <silent> K :call <SID>show_documentation()<CR>
    function! s:show_documentation()
        if (index(['vim','help'], &filetype) >= 0)
            execute 'h '.expand('<cword>')
        else
            call CocAction('doHover')
        endif
    endfunction

    inoremap <expr> <TAB> coc#pum#visible() ? coc#pum#confirm() : "\<TAB>"

    " highlight uses of the symbol under the cursor
    " responsiveness tied to :help updatetime value
    autocmd CursorHold * silent call CocActionAsync('highlight')

    "set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
endif
