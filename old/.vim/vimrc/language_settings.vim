" gdscript can't mix and match tabs and spaces, and the editor uses tabs by default
autocmd FileType gdscript3 setlocal noexpandtab
"autocmd FileType gdscript3 colorscheme mopkai

"autocmd FileType rust colorscheme mopkai

" do not use tabs in Haskell regardless of global settings
autocmd FileType haskell setlocal expandtab

" method 'syntax' doesn't work as-of writing
autocmd FileType tex setlocal foldmethod=indent
" custom verbatim syntax highlighting in LaTeX
autocmd FileType tex syn region texZone matchgroup=texStatement start="\\code|"  matchgroup=texStatement end="|" end="%stopzone\>"

" this highlights json without highlighting comments with an ugly red background
" however sometimes it is better to be explicit about invalid json such as dangling commas
autocmd FileType json syntax match Comment +\/\/.\+$+

" syntax highlighting for non-standard file types
" universal robot description format
au BufRead,BufNewFile *.urdf set syntax=xml

au BufRead,BufNewFile *.geojson set syntax=json
au BufRead,BufNewFile *.pyxbld set syntax=python

" assembly settings
" having spell checking with assembly is not pretty
autocmd FileType asm set nospell
" useful for highlighting a register
autocmd FileType asm set hlsearch


" diff/patch file settings
function SetDiffOptions()
    set nospell
    " this colorscheme has good syntax highlighting for diffs
    "colorscheme native
    "set background=dark

    "colorscheme pencil
    "set background=light

    colorscheme evening

    " for some reason this is required for the syntax highlighting to update
    set syntax=diff
endfunction
autocmd FileType diff call SetDiffOptions()

" lua / love 2d settings
" copied from https://github.com/alols/vim-love-efm/blob/master/after/ftplugin/lua.vim
" but without the unwanted change: makeprg=love .
autocmd FileType lua setlocal errorformat=Error:%*[^:]:\ %f:%l:%m,Error:\ %f:%l:%m,%f:%l:%m

" regardless of global settings: use these for python
autocmd FileType python  setlocal tabstop=4 shiftwidth=4 softtabstop=4 expandtab foldmethod=indent

" from http://www.vim.org/scripts/script.php?script_id=477
"the last line: \%-G%.%# is meant to suppress some
"late error messages that I found could occur e.g.
"with wxPython and that prevent one from using :clast
"to go to the relevant file and line of the traceback.
autocmd FileType python setlocal errorformat=
    \%A\ \ File\ \"%f\"\\\,\ line\ %l\\\,%m,
    \%C\ \ \ \ %.%#,
    \%+Z%.%#Error\:\ %.%#,
    \%A\ \ File\ \"%f\"\\\,\ line\ %l,
    \%+C\ \ %.%#,
    \%-C%p^,
    \%Z%m,
    \%-G%.%#


" don't automatically de-indent for labels, eg when typing `std::`, before the last colon is typed, `std:` looks like
" a label and will be de-indented
set cinoptions+=L0


" Abbreviations.
"
" Use <lt> to literally enter `<` rather than interpreting as a key, eg <CR> would be the enter key but <lt>CR> would not
"
" I prefer imap to iabbr because iabbr requires a space to be pressed after it, rendering it useless
"
" tip: use <C-o> to temporarily come out of insert mode and use F to search backwards if you want to put the cursor
" somewhere. Moving by h or <left> may give unreliable results depending on whether the cursor is at the end of the line
" or not.


inoremap → ->

if g:custom_abbreviations_enabled
    " Python
        autocmd FileType python inoremap `fa fig, ax = plt.subplots()
        autocmd FileType python inoremap `asp fig.tight_layout()<cr>ax.set_aspect('equal', 'datalim')
        autocmd FileType python inoremap `plt import matplotlib.pyplot as plt
        autocmd FileType python inoremap `np import numpy as np
        autocmd FileType python inoremap `pdb import pdb;pdb.set_trace()
        autocmd FileType python inoremap `i def __init__(self):<CR>
        autocmd FileType python inoremap `l lambda : <C-o>F:
        autocmd FileType python inoremap `main def main():<cr>parser = argparse.ArgumentParser()<cr>parser.add_argument()<cr>args = parser.parse_args()<cr><cr><cr><bs>if __name__ == '__main__':<cr>main()

    " C++
        autocmd FileType cpp inoremap `s std::
        autocmd FileType cpp inoremap `o std::cout << "" << std::endl;<c-o>F"
        autocmd FileType cpp inoremap `c static_cast<lt>>()<c-o>F>
        autocmd FileType cpp inoremap `t template<lt>><c-o>F>

    " Anki LaTeX
        autocmd FileType tex inoremap `no \begin{note}<CR><CR><BS>\nextfield<CR><CR>\end{note}<c-o>3k
endif
