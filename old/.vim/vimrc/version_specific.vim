if has("nvim")
    " NeoVim settings
    " gnvim specific settings are at ~/.config/nvim/ginit.vim

    " use escape to exit terminal mode (when in :terminal)
    " FZF closes the find window with <ESC> but if <ESC> is remapped then it doesn't close
    "tnoremap <Esc> <C-\><C-n>

    " terminal specific settings
    let &t_SI = "\<Esc>]50;CursorShape=1\x7"
    let &t_EI = "\<Esc>]50;CursorShape=0\x7"
    "let &t_SI = "\e[6 q"
    "let &t_EI = "\e[2 q"

    " new option for neovim: show preview of substitutions
    " split = show in place and also in a new split
    set inccommand=split

    if exists("g:neovide")
        let g:neovide_cursor_animation_length=0
        " set the title to be the current file
        " TODO: for some reason this option doesn't always work when first loading a file. It seems to be something to do with my vimrc specifically because putting the option in a clean init.vim makes the problem go away. It could also be a race condition but I was unable to reproduce with a minimal example. To create such an example I might need to start with my vimrc and cut away rather than starting from scratch
        set title
        function WorkaroundForSetTitle()
            sleep 1m
            set title
        endfunction
        autocmd BufEnter * call WorkaroundForSetTitle()
    endif


else
    " Regular Vim specific settings

    " due to nocompatible being set, Vim pauses for a bit when Esc+? is pressed when being run in a terminal
    " (where ? is some character, most annoyingly O)
    " using noesckeys will stop this but also prevent usage of the arrow keys in insert mode as the
    " terminal passes the key presses to Vim using an escape code
    set noesckeys

    if has("gui_running")
        " GVim specific settings

        set guioptions-=T " no toolbar

        " startup size and position
        "set lines=999 columns=999  " maximised
        set lines=999 columns=120    " half screen
        winpos 512 0                " in the middle

        " font is set in basics.vim and interpreted for regular gvim here
        if g:fontweight == 'l'
            let g:fontweight="Light"
        elseif g:fontweight == 'b'
            let g:fontweight="Bold"
        endif
        execute "set guifont=".escape(g:fontfamily, ' ')."\\ ".g:fontweight."\\ ".g:fontsize

    else
        " Terminal Vim specific settings

        set nospell

        set background=dark
        " allows for a line cursor in konsole (specifically, other terminals require
        " different config)
        let &t_SI = "\<Esc>]50;CursorShape=1\x7"
        let &t_EI = "\<Esc>]50;CursorShape=0\x7"

        " looks good in the terminal
        colorscheme molokai

    endif
endif
