
" leader alternatives
nmap , <leader>

nmap <leader>rc :e ~/.vimrc<CR>

" spell correction shortcut (since I pretty much always select the first option)
" choose the first potential correction
map <leader>= 1z=

" For sloppy writing and quitting
command WQ wq
command Wq wq
command W  w
command Q  q
" an alternative way to force quit
command QQ q!

" make gf work even if the file does not exist (from :help gf)
map gf :edit <cfile><cr>

" write to file that requires super user permissions (without `sudo vim` or `sudoedit` / `sudo -e`)
" needs suda.vim
cmap w!! w suda://%

" copy+paste with system clipboard (the + register)
map <leader>y "+y
map <leader>p "+p
map <leader>P "+P


nnoremap <leader>fs :FullscreenToggle<CR>
nnoremap <C-Tab> gt
nnoremap <C-S-Tab> gT


" really, <C-=> and <C-->. Does not apply to numpad +
map + :LargerFont<CR>
" Shift+<_ or - key>
map - :SmallerFont<CR>

" toggle centering the cursor vertically on the screen
nnoremap <leader>zz :let &scrolloff=999-&scrolloff<CR>

" the bang prevents jumping to the first encountered error
nnoremap <F5> :make!<CR>

" open all folds
nmap zO :set foldlevel=999<CR>

" move through camelCaseWords (note: shift arrows has the same effect)
nnoremap <C-Left>  :call search('\<\<Bar>\u', 'bW')<CR>
nnoremap <C-Right> :call search('\<\<Bar>\u', 'W')<CR>
inoremap <C-Left>  <C-o>:call search('\<\<Bar>\u', 'bW')<CR>
inoremap <C-Right> <C-o>:call search('\<\<Bar>\u', 'W')<CR>

" inspired by Emacs bindings (Ctrl-left/right to move by words also)
nnoremap <C-Up> {
nnoremap <C-Down> }

" normally scrolls by whole pages, so slow it down a bit
nnoremap <S-Up> <C-u>
nnoremap <S-Down> <C-d>

" scroll faster
nnoremap <C-j> 10j
nnoremap <C-k> 10k
vnoremap <C-j> 10j
vnoremap <C-k> 10k

" easier jumplist keybindings
nmap ( <C-o>
nmap ) <C-i>


" go to definition
"nnoremap <space> <C-]>
" list tag matches (useful for going to declaration, usually second match)
"nnoremap <leader><space> g<C-]>


" Ctrl-backspace in insert mode to delete words
imap <C-BS> <C-W>

" select the whole document
nnoremap <leader><C-a> ggVG

" see functions.vim
nmap <leader>tt :InsertTODO<CR>




" ### Plugin Keybindings ###
" LaTeX box
nnoremap <leader>ll :Latexmk<CR>
nnoremap <leader>lv :LatexView<CR>

" NERDTree
nmap <leader>nt :NERDTreeToggle<CR>
" Tagbar
nmap <leader>tb :TagbarToggle<CR>
" rainbow parentheses
nmap <leader>rp :RainbowParenthesesToggle<CR>
" better-whitespace
nmap <leader>sw :StripWhitespace<CR>
" use :ToggleWhitespace to toggle highlighting of whitespace

" colorscheme-switcher
nmap <leader>cl :NextColorScheme<CR>
nmap <leader>ch :PrevColorScheme<CR>
nmap <leader>cr :RandomColorScheme<CR>

" tabularize
" see tutorial (vimcast) here:
" http://vimcasts.org/episodes/aligning-text-with-tabular-vim/
nmap <leader>a= :Tab /=<CR>
vmap <leader>a= :Tab /=<CR>
nmap <leader>a: :Tab /:<CR>
vmap <leader>a: :Tab /:<CR>
" align using white-space, eg:
" item1:   "aligned"
" another: "aligned"
" \zs uses the character after : as alignment char
nmap <leader>as: :Tab /:\zs<CR>
vmap <leader>as: :Tab /:\zs<CR>

" Syntastic
nmap <leader>st :SyntasticToggleMode<CR>

" Ctrl+P
"let g:ctrlp_map = '<leader>gf'

" Fzf (fuzzy finder)
" <C-j> and <C-k> to navigate through results
" see <https://github.com/junegunn/fzf> for documentation on the fuzzy syntax
" GFiles respects .gitignore
nmap <leader>gf :Files<CR>
nmap <leader>gb :Buffers<CR>
nmap <leader>gl :Lines<CR>
" find in files
nmap <leader>/  :Ag<CR>
if !g:use_language_servers
    " go to symbol
    nmap <leader>gs :Tags<CR>
endif

" ### Disabled Keys ###

" Ex mode is entered by pressing Q and apparently its basically useless except for backwards compatibility
nnoremap Q <nop>
" opening help accidentally is annoying use `:help` instead
nnoremap <F1> <nop>
" K to show documentation for the symbol under the cursor (by default opens man page, see :help keywordprg)
if !g:use_language_servers
    nnoremap K <nop>
endif

