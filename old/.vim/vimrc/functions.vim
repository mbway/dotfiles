

function CustomSetGuiFont(family, size)
    " echom 'set gui font ' . a:family . ' size ' . a:size
    if has("gui_gtk")
        " echom 'gvim'
        let &guifont = a:family . ' ' . a:size
    elseif exists("g:GuiFont")
        " echom 'nvim-qt'
        call GuiFont(a:family . ":h" . a:size)
    else
        " echom 'neovide'
        let &guifont = a:family . ':h' . a:size
    endif
endfunction

function ParseGuiFont()
    if has("gui_gtk")
        " echom 'gvim'
        let s:pattern = '^\(.* \)\([1-9][0-9]*\)$'
        let fontname = substitute(&guifont, s:pattern, '\1', '')
        let size = substitute(&guifont, s:pattern, '\2', '')
        return [fontname, size]
    else
        let s:pattern = '^\(.*\):h\([1-9][0-9]*\)$'
        if exists("g:GuiFont")
            " echom 'nvim-qt'
            let fontname = substitute(g:GuiFont, s:pattern, '\1', '')
            let size = substitute(g:GuiFont, s:pattern, '\2', '')
        else
            " echom 'neovide'
            let fontname = substitute(&guifont, s:pattern, '\1', '')
            let size = substitute(&guifont, s:pattern, '\2', '')
        endif
        return [fontname, size]
endfunction


if has('nvim')
    call CustomSetGuiFont(g:fontfamily, g:fontsize)
endif


let s:minfontsize = 6
let s:maxfontsize = 32
function! AdjustFontSize(amount)
    let [family, size] = ParseGuiFont()
    if size != ''
        let newsize = size + a:amount
        if (newsize >= s:minfontsize) && (newsize <= s:maxfontsize)
            call CustomSetGuiFont(family, newsize)
        endif
    endif
endfunction



function! LargerFont()
  call AdjustFontSize(2)
endfunction
command! LargerFont call LargerFont()

function! SmallerFont()
  call AdjustFontSize(-2)
endfunction
command! SmallerFont call SmallerFont()

" toggle both word wrap and the visibility of the bottom scroll bar
" from https://stackoverflow.com/a/9002859
function! ToggleHorizontalScroll()
    if &wrap
        set nowrap
        set guioptions+=b
    else
        set wrap
        set guioptions-=b
    endif
endfunction
command! ToggleHorizontalScroll call ToggleHorizontalScroll()


function! InsertTODO()
    if stridx(&syntax, 'markdown') >= 0  " to affect markdown, markdown.pandoc etc
        let comment='**TODO(matt):**  '
    else
        let comment=substitute(&commentstring, '\s*%s\s*', ' TODO(matt): ', '')
    endif
    execute "normal a" . comment
    call feedkeys("a")
endfunction
command! InsertTODO call InsertTODO()


" Python calculator
if has('python3')
    :command! -nargs=+ Calc :py3 print(<args>)
    :py3 from math import *
elseif has('python')
    :command! -nargs=+ Calc :py print <args>
    :py from math import *
endif

" :Open and :Google commands
if has('unix')
    let g:open_command = 'xdg-open'
else
    let g:open_command = 'start'
endif
command! -nargs=+ -complete=file_in_path Open call system(g:open_command . ' "' . expand("<args>") . '" &')
command! -nargs=+ Google call system(g:open_command . ' "https://www.google.com/search?q=' . expand("<args>") . '" &')


function CheckPythonSyntax()
    " Write the current buffer to a temporary file, check the syntax and
    " if no syntax errors are found, write the file
    let curfile = bufname("%")
    let tmpfile = tempname()
    silent execute "write! ".tmpfile
    " default to python 3, but if the shebang is python2 use that instead
    if getline(1) =~? '#!.*python2.*'
        let pycmd = "python2"
    else
        let pycmd = "python3"
    endif
    let output = system(pycmd." -c \"__import__('py_compile').compile(r'".tmpfile."')\" 2>&1")
    if output != ''
        " Make sure the output specifies the correct filename
        let output = substitute(output, fnameescape(tmpfile), fnameescape(curfile), "g")
        echo output
    endif
    write
    " Delete the temporary file when done
    call delete(tmpfile)
endfunction


function OnWritePython()
    call CheckPythonSyntax()
    "call Flake8()
endfunction
autocmd! BufWriteCmd *.py call OnWritePython()


" pretty-format the current buffer
command! PrettyJSON :%!python3 -m json.tool
command! CompactJSON :%!python3 -m json.tool --no-indent


command! WordCount :w !wc -w



" reverse the python stack trace so that the inner most exception comes first
" from https://stackoverflow.com/a/22465734
function! s:InnermostExceptionInQFList()
  let s:num = 0
  for item in getqflist()
    if item.lnum > 0
      let s:num += 1
    endif
  endfor
  if s:num > 0
    try
      silent execute(s:num . 'cnext')
    catch /E553:/
      " E553: No more elements
    endtry
  endif
  silent execute('wincmd w')
endfunction

autocmd FileType python :autocmd QuickfixCmdPost * call s:InnermostExceptionInQFList()


" This is my first Vim function from scratch :)
" open a 'todo list' which is just the results of searching for 'todo' in the document
function TODO()
    " lvim = lvimgrep
    " l => opens results in the 'location list'
    :lvimgrep todo %
    " focus on the location list
    :lopen
endfunction
" functions can only be called with :call FuncName, so create a command corresponding to it
command! TODO call TODO()
