-- based on https://github.com/zc62/mpv-scripts

-- mpv issue 5222
-- Automatically set loop-file=inf for duration <= given length. Default is 5s
-- Use autoloop_duration=n in script-opts/autoloop.conf to set your preferred length
-- Alternatively use script-opts=autoloop-autoloop_duration=n in mpv.conf (takes priority)
-- Also disables the save-position-on-quit for this file, if it qualifies for looping.


require 'mp.options'
local utils = require 'mp.utils'

function string.startswith(haystack, needle)
   return string.sub(haystack, 1, string.len(needle)) == needle
end

function getOption()
    local options = {autoloop_duration = 15}
    read_options(options)
    autoloop_duration = options.autoloop_duration
end

function set_loop()
    local env = utils.get_env_list()
    for key, value in pairs(env) do
        if string.startswith(value, 'MPV_NO_AUTO_LOOP') then
            print('disabled auto loop with MPV_NO_AUTO_LOOP')
            return
        end
    end

    local duration = mp.get_property_native("duration")
    if not duration then
        return
    end

    if not mp.get_property_native("loop-file") and duration <= autoloop_duration then
        mp.set_property_native("loop-file", true)
        mp.set_property_bool("file-local-options/save-position-on-quit", false)
    end
end


getOption()
mp.register_event("file-loaded", set_loop)
