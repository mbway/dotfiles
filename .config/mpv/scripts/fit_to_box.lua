box_size = {800, 800}

function fit_to_box()
    scale_h = box_size[1] / mp.get_property("width")
    scale_v = box_size[2] / mp.get_property("height")
    scale = math.min(scale_h, scale_v)
    mp.set_property("window-scale", scale)
    mp.set_property_native("geometry", "50%:50%")
end

mp.add_key_binding("g", "Fit_To_Box", fit_to_box)
