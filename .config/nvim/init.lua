-- toggle whether to include 'heavyweight' IDE functionality like language servers
use_language_servers = true and os.getenv('VIM_NO_LANGUAGE_SERVERS') ~= '1'

require("user.utils")          -- ./lua/user/utils.lua
require("user.basics")         -- ./lua/user/basics.lua
require("user.plugins")        -- ./lua/user/plugins.lua
require("user.plugin_install") -- ./lua/user/plugin_install.lua
require("user.functions")      -- ./lua/user/functions.lua
require("user.lsp")            -- ./lua/user/lsp.lua
require("user.preferences")    -- ./lua/user/preferences.lua

--[[

# Debugging #
- :help lua-guide or see <https://neovim.io/doc/user/lua.html>
- this tracking issue has a good table of vimscript -> lua conversions: https://github.com/neovim/neovim/issues/18393
- run with --clean to disable loading this init file
- :checkhealth to check if there are any errors
- :messages to show output of vim.print
- `do return end` is required instead of just `return` to exit early from a script


# Bootstrapping #
Lazy.nvim will bootstrap automatically and install the plugins automatically.

Set the keyboard repeat delay and rate on the system to approximately:
Delay 210ms
Rate 30 repeats/sec or 33ms delay interval between
for Ubuntu/Gnome
/usr/bin/gsettings set org.gnome.desktop.peripherals.keyboard delay 210
/usr/bin/gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 33


--]]



--[[
TODO:
- configure ruff_format autoformatter to use preview style. currently fails if any arguments provided
- debugging nvim-dap, nvim-dap-python, nvim-dap-ui
- language server problems
    - inconsistent autocomplete:
        - python autocompletes brackets but places cursor after the brackets
        - rust <tab> cancels the autocomplete when autocompleting a function but not a struct or variable
    - autocomplete should insert brackets automatically when autocompleting a method
- trailing whitespace issues
    - should be disabled in terminal (eg run fzf, looks bad)
    - sometimes highlights on the current line
    - sometimes doesn't highlight trailing whitespace
- toggle horizontal scroll (from old vim config)
    - nvim-scrollview does not support horizontal scrolling
- stop automatically inserting a newline after 120 characters
- https://github.com/mfussenegger/nvim-dap
-     https://github.com/mfussenegger/nvim-dap-python
- https://github.com/nvim-neotest/neotest
- https://github.com/mg979/vim-visual-multi
- optimize imports if the language server supports it
    - looks like a custom function will be required, eg https://stackoverflow.com/a/67851340
- snippets (luasnip or something else)
- make keybindings consistent between neovim, vscode and pycharm
- when writing html without indentation, vim insists on indenting as you are writing

--]]
