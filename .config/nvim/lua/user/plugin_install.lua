-- bootstrap lazy vim
-- https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
-- bootstrap end


plugins = {}

vim.list_extend(plugins, color_scheme_plugins)
vim.list_extend(plugins, basic_plugins)
vim.list_extend(plugins, ui_plugins)
vim.list_extend(plugins, basic_ide_plugins)
if use_language_servers then
    vim.list_extend(plugins, language_server_plugins)
end

require("lazy").setup(plugins)
