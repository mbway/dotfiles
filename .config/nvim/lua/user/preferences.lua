-- Try out fonts at
-- https://www.programmingfonts.org/
-- https://github.com/ProgrammingFonts/ProgrammingFonts
-- For reference, VSCode font priorities:
--   Monaco, Menlo, Consolas, Droid Sans Mono, Inconsolata, Courier New, monospace

local utils = require('user.utils')

vim.opt.background = 'dark'
vim.cmd.colorscheme(
    'catppuccin-macchiato'
    --'solarized'
    --'monokai'
)

-- note: can use `:lua SetGuiFont(...)` to change the font at runtime
SetGuiFont(
    'Source Code Pro',
    --'Ubuntu Mono',
    --'Code New Roman',
    --'Hack',
    --'Mononoki'  -- very similar to SourceCodePro but more compact horizontally
    --'Monospace'
    14
)

if vim.g.neovide then
    --vim.g.neovide_cursor_animation_length = 0
    --vim.g.neovide_transparency = 0.95
    --vim.g.neovide_scroll_animation_length = 0
    --vim.g.neovide_refresh_rate = 120
end


-- Run :map, :imap, :vmap to view current keybindings

-- Digraphs
-- in insert mode, press C-k then two keys to insert a unicode symbol
-- Run `:help digraph-table` to open a list of supported digraphs that can be searched
-- regular:  $$  £
-- greek:    a*  α   b*  β   g*  γ   d*  δ   h*  θ
-- logic:    )U  ∪   (U  ∩   (-  ∈   (=  ∈   TE  ∃   FA  ∀
-- calculus: dP  ∂   In  ∫   +Z  ∑
-- misc:     00  ∞   Ob  ∘   RT  √

---------------------
-- Navigation Keys --
---------------------
vim.keymap.set({'n', 'v'}, '<C-j>', '10j')
vim.keymap.set({'n', 'v'}, '<C-k>', '10k')
vim.keymap.set({'n', 'v'}, '<C-Up>', '{')
vim.keymap.set({'n', 'v'}, '<C-Down>', '}')
vim.keymap.set({'n', 'v'}, '<S-Up>', '<C-u>')
vim.keymap.set({'n', 'v'}, '<S-Down>', '<C-d>')

---------------
-- Misc Keys --
---------------
-- Ctrl-Backspace to delete words in insert mode
vim.keymap.set('i', '<C-BS>', '<C-W>')

-- insert literal tab
vim.keymap.set('i', '<S-TAB>', '<C-V><TAB>')

-- disable F1 from opening help
vim.keymap.set('n', '<F1>', '<nop>')

-- fullscreen toggle
vim.keymap.set('n', '<F11>', FullscreenToggle)

-- adjust font size
-- really, <C-=> and <C-->. Does not apply to numpad +
vim.keymap.set('n', '+', function() AdjustGuiFont(2) end)
vim.keymap.set('n', '-', function() AdjustGuiFont(-2) end)

-- tabs
-- (tabs can be moved and closed with the mouse)
--vim.keymap.set('n', '<C-t>', vim.cmd.tabnew)
vim.keymap.set('n', '<C-Tab>', vim.cmd.tabnext) -- gt
vim.keymap.set('n', '<C-S-Tab>', vim.cmd.tabprevious) -- gT

-- gf goes to the file under the cursor but only if it already exists.
-- This binding creates the file if it does not exist. Best to use a binding other than gf because
-- other bindings like gd are close and a typo would create a new file which would be confusing/annoying.
vim.keymap.set('n', 'gF', GoToOrCreateFileUnderCursor)

-- sloppy write and quit
-- (less of a concern now that I have : and ; switched)
vim.keymap.set('c', 'WQ', 'wq')
vim.keymap.set('c', 'Wq', 'wq')

-- change indent without leaving visual mode
vim.keymap.set('v', '<', '<gv', { noremap = true, silent = true })
vim.keymap.set('v', '>', '>gv', { noremap = true, silent = true })

-- use escape to return to normal mode from terminal mode unless the active application uses escape.
-- <C-\><C-n> can always be used to return to normal mode
local apps_that_use_escape = utils.Set:new({'nvim', 'fzf'})
vim.keymap.set('t', '<Esc>', function() TerminalEscape(apps_that_use_escape) end, { noremap = true, silent = true })


-----------------
-- Leader Keys --
-----------------
vim.g.mapleader = ','

-- Toggle centering the cursor vertically at all times
vim.keymap.set('n', '<leader>zz', function() vim.wo.scrolloff = 999 - vim.wo.scrolloff end)
-- open all folds
vim.keymap.set('n', '<leader>z0', function() vim.opt.foldlevel=99 end)

vim.keymap.set('n', '<leader>nt', function() vim.cmd.Neotree('toggle') end)

vim.keymap.set('n', '<leader>tt', InsertTODO)

vim.keymap.set('n', '<leader><CR>', 'o# %%<CR>')

-- go to files
vim.keymap.set('n', '<leader>gf', function() vim.cmd.Telescope('find_files') end)
-- go to files with outstanding changes in git
vim.keymap.set('n', '<leader>gg', function() vim.cmd.Telescope('git_files') end)
-- go to symbol
vim.keymap.set('n', '<leader>gs', function() vim.cmd.Telescope('lsp_dynamic_workspace_symbols') end)
-- go to errors
vim.keymap.set('n', '<leader>ge', function() vim.cmd.Telescope('diagnostics') end)
-- find string in files
vim.keymap.set('n', '<leader>/', function() vim.cmd.Telescope('live_grep') end)  -- requires ripgrep

-- enter a digraph from normal mode. Also works in vscode
vim.keymap.set('n', '<leader>\\', function()
  local msg = 'Digraph: '
  print(msg)
  local a = vim.fn.getcharstr()
  print(msg .. a)
  local b = vim.fn.getcharstr()
  print(' ')
  local digraph = vim.fn.digraph_get(a .. b)
  vim.api.nvim_put({ digraph }, '', false, true)
end)



vim.keymap.set('n', '<leader>ca', ShowCodeActions)
vim.keymap.set('n', '<A-Enter>', ShowCodeActions)
vim.keymap.set('n', '<F2>', vim.lsp.buf.rename)
vim.keymap.set('i', '<C-9>', vim.lsp.buf.signature_help)
-- shows documentation for symbol under cursor (by default K opens the vim help for the symbol under the cursor)
vim.keymap.set('n', 'K', vim.lsp.buf.hover)

-- run autoformatter
vim.keymap.set('n', '<leader>f', function()
    require('conform').format({ async = true, lsp_fallback = true })
end)


vim.keymap.set('n', '<space>', GoToDefinitionOrReferences)
vim.keymap.set('n', 'gd', function() vim.cmd.Telescope('lsp_definitions') end)
vim.keymap.set('n', 'gi', function() vim.cmd.Telescope('lsp_implementations') end)
vim.keymap.set('n', 'gt', function() vim.cmd.Telescope('lsp_type_definitions') end)
vim.keymap.set('n', 'gr', function() vim.cmd.Telescope('lsp_references') end)


-----------------
-- Plugin Keys --
-----------------

-- suda.vim
-- write to file that requires super user permissions
-- (note: if the directory has write permissions then `w!` is sufficient to force a write)
vim.keymap.set('c', 'w!!', vim.cmd.SudaWrite)

-- better-whitespace
vim.keymap.set('n', '<leader>sw', vim.cmd.StripWhitespace)


--------------
-- Commands --
--------------

vim.api.nvim_create_user_command('FullscreenToggle', function(opts) FullscreenToggle() end, {})
vim.api.nvim_create_user_command('ToggleBreadcrumbs', function(opts) ToggleBreadcrumbs() end, {})
-- with hlsearch enabled, matches for the last search are highlighted. This will clear the last
-- search string to clear the highlighting without using :set hlsearch!
vim.api.nvim_create_user_command('ClearSearch', function(opts) vim.fn.setreg('/', '') end, {})

-- print working directory. Can also use `:!pwd`
vim.api.nvim_create_user_command('Pwd', function(opts) vim.print(vim.fn.getcwd()) end, {})

-- open the neovim configuration file.
-- (this used to be <leader>rc but <leader>r is useful for other purposes and having rc would prevent <leader>r from
-- being executed without pausing)
vim.api.nvim_create_user_command('OpenNeovimConfig',
    function(opts)
        vim.cmd.e(vim.fn.stdpath('config') .. '/init.lua')
    end,
    {}
)

-- use :JsonFmt to format the entire document or visually select some text and use :'<,'>JsonFmt
--
-- JsonFmt takes a single optional argument:
--
--   p | pretty    format with 2 spaces indent
--   pp            format with 4 spaces indent
--   c | compact   format on a single line with some spacing
--   cc            format on a single line with no spacing at all
vim.api.nvim_create_user_command('JsonFmt',
    function(opts) JsonFmt(opts.args, opts.range) end,
    { nargs = '?', range = true }
)

