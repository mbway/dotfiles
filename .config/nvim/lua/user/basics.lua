-- General Functionality Preferences

-- see https://neovim.io/doc/user/vim_diff.html#nvim-defaults

vim.opt.mouse = 'a'  -- enable mouse support
vim.opt.title = true  -- set the window title to the filename
vim.opt.number = true -- show line numbers
vim.opt.clipboard = 'unnamedplus'  -- use system clipboard for everything
vim.opt.cursorline = true  -- highlight the current line
vim.opt.scrolloff = 5  -- show at least N lines above or below the cursor when navigating
vim.opt.sidescrolloff = 5
vim.opt.undolevels = 2000 -- length of undo history (default 1000)
vim.opt.listchars = {  -- non-printable characters to show with `set list`
    tab      = '» ',
    trail    = '·',
    eol      = '⤶',
    nbsp     = '+',
    extends  = '◀',
    precedes = '▶',
}

-- search
-- note on regex:
-- by default: no special characters have special meaning, they are interpreted
-- literally. To enable the special meaning, escape with backslashes. To get the
-- equivalent of grep -E (extended regex) put \v at the start (standing for 'very magic')
vim.opt.hlsearch = true  -- highlight search matches. Clear with :ClearSearch
vim.opt.showmatch = true -- briefly highlight a matching bracket when typing a new one
vim.opt.ignorecase = true -- (required to use smartcase)
vim.opt.smartcase = true -- case insensitive if query contains no upper case. \C \c to override
vim.opt.inccommand = 'split' -- show substitutions in a new split

-- wrapping
vim.opt.textwidth = 120  -- for text wrapping with gq
vim.opt.colorcolumn = '120'  -- highlighted columns
vim.opt.wrap = true
vim.opt.linebreak = true -- only break at characters in 'breakat'
vim.opt.formatoptions = {'q'} -- only wrap with gq, no auto-wrap
vim.opt.linebreak = true -- only break lines when wrapping at the edge of the screen on characters in `:set breakat?`

vim.api.nvim_create_autocmd('FileType', {
    pattern = '*',
    -- default is 1croql
    callback = function()
        vim.opt_local.formatoptions:remove('o') -- do not insert a comment when using o to insert a newline below the current one
    end
})

-- folding
vim.opt.foldmethod = 'syntax'
vim.opt.foldenable = true
vim.opt.foldlevelstart = 99

-- tabs
vim.opt.tabstop = 4  -- display <tab> as this many spaces
vim.opt.shiftwidth = 4  -- move this many columns when << or >> (should be equal to tabstop)
vim.opt.softtabstop = 4 -- the number of columns to use for an indentation (should be equal to tabstop)
vim.opt.expandtab = true -- insert spaces with <tab>. Insert a literal tab with Ctrl-V<tab>

-- spelling
-- note: add good spellings with zg and wrong spellings with zw
-- BUG:
--     in pycharm terminal open neovim with my config with spell = true.
--     Type `a<space>` in insert mode.
--     displayed text will be `::::::-a`
--     if this is no longer the case this workaround can be removed.
--     Note that spell = true with an otherwise empty config does not have this bug so could investigate further
--     the PYCHARM_HOSTED variable has to be set manually in `Settings > Terminal > Environment Variables`
vim.opt.spell = true and os.getenv('PYCHARM_HOSTED') ~= '1'
vim.opt.spellfile = vim.fn.expand('~/.myDictionary.utf-8.add')
vim.opt.spelllang = 'en_gb'  -- built-in dictionaries

-- state
-- $XDG_STATE_HOME = ~/.local/state/
--
-- backupdir: store myfile.txt~ files (backup from before editing)
--     enabled by vim.opt.writebackup (default on)
-- directory: store myfile.txt.swp files (unsaved changes)
--     enabled by vim.opt.swapfile (default on)
-- undodir:   store myfile.txt.un~ files (undo tree)
--     enabled by vim.opt.undofile (default off)
--
-- $XDG_STATE_HOME/nvim/shada stores state such as command line and search history (see :help shada)

-- note: // means that directory information will be encoded into the filename
vim.opt.backupdir = vim.fn.stdpath('state') .. '/backup//'
vim.opt.directory = vim.fn.stdpath('state') .. '/swap//'
vim.opt.undodir = vim.fn.stdpath('state') .. '/undo//'

-- language specific

vim.api.nvim_create_autocmd('FileType', {
    pattern = 'python',
    callback = function()
        vim.api.nvim_buf_set_option(0, 'tabstop', 4)
        vim.api.nvim_buf_set_option(0, 'shiftwidth', 4)
        vim.api.nvim_buf_set_option(0, 'softtabstop', 4)
        vim.api.nvim_buf_set_option(0, 'expandtab', true)
        vim.w.foldmethod = 'indent'
    end
})

-- gdscript can't mix and match tabs and spaces, and the editor uses tabs by default
vim.api.nvim_create_autocmd('FileType', {
    pattern = 'gdscript',
    callback = function() vim.api.nvim_buf_set_option(0, 'expandtab', false) end
})

vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile'}, {
    pattern = '*.geojson',
    callback = function() vim.api.nvim_buf_set_option(0, 'syntax', 'json') end
})

vim.api.nvim_create_autocmd({'BufRead', 'BufNewFile'}, {
    pattern = '*.json',
    callback = function() vim.api.nvim_buf_set_option(0, 'filetype', 'jsonc') end
})

