-- User facing functions (i.e. not utilities)

local utils = require('user.utils')
local Path = require('plenary.path')

-- show code actions in a popup if the plugin is available and otherwise as a regular option list
function ShowCodeActions()
    local has_actions_preview, actions_preview = pcall(require, 'actions-preview')
    if has_actions_preview then
        actions_preview.code_actions()
    else
        vim.lsp.buf.code_action()
    end
end


function InsertTODO()
    local prefix = 'TODO(matt):'

    -- have to use `filetype`, the `syntax` is not set when using treesitter
    local syntax = vim.api.nvim_buf_get_option(0, 'filetype')
    local comment
    if syntax:find("markdown") then -- affect markdown, markdown.pandoc etc
        comment = '**' .. prefix .. '** '
    else
        local comment_pattern
        if syntax == 'cpp' then
            -- uses /* */ style comments by default
            comment_pattern = '// %s'
        elseif syntax == 'lisp' then
            comment_pattern = ';; %s'
        elseif syntax == 'sql' then
            comment_pattern = '-- %s'
        else
            comment_pattern = vim.api.nvim_buf_get_option(0, 'commentstring')
        end

        if comment_pattern == '' then
            comment = '# ' .. prefix .. ' '
        else
            comment = comment_pattern:gsub("%s*%%s%s*", ' ' .. prefix .. ' ')
        end
    end
    vim.fn.execute('normal a' .. comment)
    vim.fn.feedkeys('a')
end


function FullscreenToggle()
    if vim.g.neovide ~= nil then
        vim.g.neovide_fullscreen = not vim.g.neovide_fullscreen
    else
        vim.print('Unknown GUI')
    end
end

function SetGuiFont(font_family, font_size)
    if font_size == nil then
        font_size = ParseGuiFont()[2]
    end
    if vim.fn.has('gui_running') == 1 then
        if vim.g.neovide ~= nil then
            vim.o.guifont = font_family .. ':h' .. font_size
        else
            vim.print('Unknown GUI')
        end
    end
end

function ParseGuiFont()
    local font_name, font_size
    if vim.g.neovide ~= nil then
        local pattern = '^(.*):h([1-9][0-9]*)$'
        font_name = vim.o.guifont:gsub(pattern, '%1')
        local font_size_str = vim.o.guifont:gsub(pattern, '%2')
        font_size = tonumber(font_size_str)
    else
        vim.print('Unknown GUI')
    end
    return {font_name, font_size}
end

function AdjustGuiFont(amount)
    local min_size = 4
    local max_size = 40

    local font_family, font_size = unpack(ParseGuiFont())
    if font_size ~= nil then
        font_size = font_size + amount
        if font_size < min_size or font_size > max_size then
            font_size = nil
        end
    end

    if font_size ~= nil then
        SetGuiFont(font_family, font_size)
        -- if printed immediately it seems to get cleared
        vim.defer_fn(function() vim.print('font size: ' .. font_size) end, 50)
    end
end


-- replacement for the default `gf` keybinding that creates a new file if one does not already exist.
-- better than the `:edit <cfile><cr>` suggestion from `:help gf` because that only checks whether the file exists
-- in the neovim cwd so links relative to the open file cannot be followed.
function GoToOrCreateFileUnderCursor()
    local ok, result = pcall(vim.cmd, 'normal! gf')
    if not ok then
        local hover_path = vim.fn.expand('<cfile>')
        if hover_path ~= '' then
            if not Path:new(hover_path):is_absolute() then
                local current_buffer_path = Path:new(vim.api.nvim_buf_get_name(0)):parent()
                hover_path = tostring(current_buffer_path:joinpath(hover_path):normalize())
            end
            vim.cmd.edit(hover_path)
        end
    end
end


-- if the symbol under the cursor is a definition then show/go to the uses of the symbol.
-- if the symbol is a use and not a definition, then go to the definition
function GoToDefinitionOrReferences()
    function inside_range(result, buffer_num, window_num)
        local filepath = vim.api.nvim_buf_get_name(buffer_num)
        -- targetUri / targetRange is used when hovering over an item in an import statement
        local uri = result.uri or result.targetUri
        if vim.uri_to_fname(uri) ~= filepath then
            return false
        end
        local range = result.range or result.targetRange
        local line, char = unpack(vim.api.nvim_win_get_cursor(window_num))
        line = line - 1  -- make 0-indexed
        if line < range.start.line or (line == range.start.line and char < range.start.character) then
            return false
        end
        if line > range['end'].line or (line == range['end'].line and char > range['end'].character) then
            return false
        end
        return true
    end

    -- the lsp interaction stuff is based on the telescope.nvim source code
    local buffer_num = vim.api.nvim_get_current_buf()
    local window_num = vim.api.nvim_get_current_win()
    local params = vim.lsp.util.make_position_params(window_num)
    vim.lsp.buf_request(buffer_num, "textDocument/definition", params, function(err, result, ctx, _)
        if err then
            vim.api.nvim_err_writeln("error getting lsp definitions")
            return
        end

        -- if there is exactly one definition result and we are already inside it: go to references,
        -- otherwise: go to definition
        local go_to_definition = false
        if result == nil or #result ~= 1 then
            go_to_definition = true
        else
            local inside_definition = inside_range(result[1], buffer_num, window_num)
            go_to_definition = not inside_definition
        end

        if go_to_definition then
            vim.cmd.Telescope('lsp_definitions')
        else
            vim.cmd.Telescope('lsp_references')
        end
    end)
end

-- toggle breadcrumbs at the top of the screen showing the current class/method using the 'navic' plugin
function ToggleBreadcrumbs()
    local has_navic, _ = pcall(require, 'nvim-navic')
    if has_navic then
        if vim.o.winbar == '' then
            -- from https://github.com/SmiteshP/nvim-navic
            vim.o.winbar = "%{%v:lua.require'nvim-navic'.get_location()%}"
        else
            vim.o.winbar = ''
        end
    else
        vim.print('navic not found')
    end
end

-- when called from terminal mode: return to normal mode unless the active application uses escape
--
-- neovim does not map escape to returning to normal mode from terminal mode because some terminal applications
-- may consume escape <https://github.com/neovim/neovim/issues/7648>
--
-- based on https://github.com/sychen52/smart-term-esc.nvim
function TerminalEscape(escape_consuming_apps)
    local terminal_pid = vim.b.terminal_job_pid
    if terminal_pid == nil then
        vim.print('not inside a terminal buffer')
        return
    end

    local function process_uses_escape(pid)
        local proc = vim.api.nvim_get_proc(pid)
        if escape_consuming_apps:contains(proc.name) then
            return true
        end
        for _, child in ipairs(vim.api.nvim_get_proc_children(pid)) do
            if process_uses_escape(child) then
                return true
            end
        end
        return false
    end

    local keys
    if process_uses_escape(terminal_pid) then
        keys = '<Esc>'
    else
        keys = '<C-\\><C-n>'
    end

    vim.fn.feedkeys(utils.replace_termcodes(keys), 'n')
end


function JsonFmt(args, range)
    local cmd_args, cmd_range

    if args == '' or args == 'p' or args == 'pretty' then
        cmd_args = '--indent 2'
    elseif args == 'pp' then
        cmd_args = '--indent 4'
    elseif args == 'c' or args == 'compact' then
        cmd_args = '--no-indent'
    elseif args == 'cc' then
        cmd_args = '--compact'
    end

    if range == 2 then
        cmd_range = "'<,'>"
    elseif range == 0 then
        cmd_range = '%'
    end

    vim.cmd(':' .. cmd_range .. '!python3 -m json.tool ' .. cmd_args)
end
