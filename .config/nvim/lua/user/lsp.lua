if not use_language_servers then
    return
end


-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
--
-- Use :LspInfo to get the status of whether a language server is attached to the current buffer
local lsp = require('lspconfig')

-- enable debug and use :LspLog to open the lsp log
-- vim.lsp.set_log_level("debug")


local has_cmp_nvim_lsp, cmp_nvim_lsp = pcall(require, 'cmp_nvim_lsp')
local capabilities
if has_cmp_nvim_lsp then
    capabilities = cmp_nvim_lsp.default_capabilities()
else
    capabilities = vim.lsp.protocol.make_client_capabilities()
end

local has_navic, navic = pcall(require, 'nvim-navic') -- breadcrumb plugin

local on_attach = function(client, bufnr)
    if has_navic then
        if client.server_capabilities.documentSymbolProvider then
            navic.attach(client, bufnr)
        end
    end
end

-- requires bash-language-server
lsp.bashls.setup{
    capabilities = capabilities,
    on_attach = on_attach,
}

-- requires clangd
-- requires `compile_commands.json` at the root of the project (symlink if necessary)
lsp.clangd.setup{
    capabilities = capabilities,
    on_attach = on_attach,
}

-- requires rust-analyzer
lsp.rust_analyzer.setup{
    capabilities = capabilities,
    on_attach = on_attach,
}

-- requires running godot instance (connects to listening port)
lsp.gdscript.setup{
    capabilities = capabilities,
    on_attach = on_attach,
}

-- python language servers:
-- - pyright (microsoft): open source. (https://github.com/microsoft/pyright)
--     Does type checking and a few other things. Does not do import helping
-- - pylance (microsoft): closed source
--     builds on pyright to offer more lsp functionality
-- - jedi_language_server (https://github.com/pappasam/jedi-language-server)
--     Focused on only supporting jedi functionality
--     Does code actions but no type checking or import helping or even basic inspections like missing symbols
-- - pylsp (https://github.com/python-lsp/python-lsp-server)
--     Extensible, combines open source tools like jedi, ruff, rope etc together
-- - pylyzer (https://github.com/mtshiba/pylyzer)
--     very experimental. Only supports type checking


local use_pyright = true

if use_pyright then

    lsp.pyright.setup({
        capabilities = capabilities,
        on_attach = on_attach,
    })

else

    -- TODO(matt): solve auto import issues:
    --  - auto import is using the pipx environment of the language server, not the current virtualenv, other plugins
    --  like jedi are using the correct virtualenv I think because I can go to definition of something not installed in
    --  the pipx environment. I know rope is using the pipx environment by looking at the .ropeproject symbols cache
    --  - after enabling, rope_autoimport, opening the code actions list can take ~10 seconds each time.
    --    the language server also seems very slow to refresh even though ruff and mypy are quite snappy
    --  - store the rope cache somewhere other than the cwd
    --  - make auto import suggestions higher priority in the code actions list

    -- <https://github.com/python-lsp/python-lsp-server/blob/develop/CONFIGURATION.md>
    --
    -- pipx install python-lsp-server[rope]
    -- pipx inject python-lsp-server pylsp-mypy python-lsp-ruff
    lsp.pylsp.setup({
        capabilities = capabilities,
        on_attach = on_attach,
        settings = {
            pylsp = {
                plugins = {
                    rope_autoimport = {
                        enabled = false,
                        memory = false -- when false, creates sqlite database cache in cwd
                    },
                    rope_completion = { enabled = false },
                    mypy = {
                        enabled = true,
                        strict = true
                    },
                    ruff = {
                        enabled = true,

                        -- default settings if pyproject.toml does not override
                        lineLength = 120,
                        preview = true,  -- preview formatting style
                        extendIgnore = { "Q000" },
                        targetVersion = "py311"
                    },

                    jedi_completion = { enabled = true, include_params = true },
                    jedi_definition = { enabled = true },
                    jedi_hover = { enabled = true },
                    jedi_references = { enabled = true },
                    jedi_signature_help = { enabled = true },
                    jedi_symbols = { enabled = true },

                    autopep8 = { enabled = false },
                    flake8 = { enabled = false },
                    mccabe = { enabled = false },
                    preload = { enabled = false },
                    pycodestyle = { enabled = false },
                    pydocstyle = { enabled = false },
                    pyflakes = { enabled = false },
                    pylint = { enabled = false },
                    yapf = { enabled = false }
                }
            }
        }
    })

end
