--[[
Plugin External Prerequisites:

- autoswap
    - wmctrl
- telescope
    - ripgrep
    - fd
- langauge servers
    - see ./lsp.lua
    - ruff

--]]


color_scheme_plugins = {
    {
        "catppuccin/nvim",
        name = "catppuccin",
        lazy = false,
        priority = 1000,
    },
    {
        "altercation/vim-colors-solarized",
        name = "solarized",
        lazy = false,
        priority = 1000,
        cond = false,  -- disabled
    },
    {
        'ku1ik/vim-monokai',
        name = "monokai",
        lazy = false,
        priority = 1000,
        cond = false,  -- disabled
    },
}

-- plugins that provide 'basic' functionality
basic_plugins = {
    {
        -- lua standard library
        'nvim-lua/plenary.nvim'
    },
    {
        -- keybindings for surrounding selection with a character
        "tpope/vim-surround",
    },
    {
        -- use `.` to repeat plugin actions. (for vim-surround and others)
        "tpope/vim-repeat",
    },
    {
        -- used for saving file with sudo
        "lambdalisue/suda.vim",
        lazy = true,
        cmd = { "SudaRead", "SudaWrite" },  -- load when these functions are called
    },
    {
        -- adds :Rename command to rename current file
        "danro/rename.vim",
        lazy = true,
        cmd = { "Rename" },
    },
    {
        -- align by selecting a range and using `Tab`. Eg to align equals signs: :'<,'>Tab /=
        "godlygeek/tabular",
        lazy = true,
        cmd = { "Tabularize" },
    },
    {
        -- highlight trailing whitespace and provide StripWhitespace
        "ntpeters/vim-better-whitespace",
        config = function()
            vim.g.better_whitespace_guicolor = '#aa2222'
            vim.g.strip_whitespace_on_save = 0
        end,
    },
    {
        -- search for visual selected text with * and # (useful for Unicode text)
        "bronson/vim-visual-star-search",
    },
    {
        -- when opening a file which is already open: switch to that window instead. (requires wmctrl)
        -- note: doesn't work if the buffer isn't focused because it uses the window title to determine what to switch to
        -- 'gioele/vim-autoswap'  " use my fork until it is merged upstream
        'mbway/vim-autoswap'
    },
    {
        -- adds StartupTime command to inspect startup time
        'dstein64/vim-startuptime',
        cmd = { 'StartupTime' }
    },
    {
        -- handle large files by disabling features when loading them
        'LunarVim/bigfile.nvim',
        config = function()
            require('bigfile').setup({
                filesize = 2,  -- MiB

            })
        end
    }
}

ui_plugins = {
    {
        -- adds a scrollbar
        "dstein64/nvim-scrollview",
        config = function()
            require('scrollview').setup({})
        end
    },
    {
        -- show status of each line in git (added, deleted etc) in the gutter
        'lewis6991/gitsigns.nvim',
        config = function()
            require('gitsigns').setup({})
        end
    },
    {
        -- provides a 'diff view' to stage and unstage git changes as well as some other features
        -- such as viewing the history of the current file
        'sindrets/diffview.nvim',
        lazy = true,
        cmd = { 'DiffviewOpen', 'DiffviewFileHistory', },
    },
    {
        -- file browser
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v3.x",
        dependencies = {
          "nvim-lua/plenary.nvim",
          "nvim-tree/nvim-web-devicons",
          "MunifTanjim/nui.nvim",
        },
        lazy = true,
        cmd = { "Neotree" },  -- load when this function is called
        config = function()
            require('neo-tree').setup({
                close_if_last_window = true,
            })
        end,
    },
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        config = function()
            -- all the default themes are way too saturated and eye catching. This custom theme is less distracting.
            local custom_theme = require('lualine.themes.iceberg_dark')
            custom_theme.normal.a.bg = '#5d606c'
            custom_theme.insert.a.bg = '#5f6c5e'
            custom_theme.visual.a.bg = '#60556c'

            require('lualine').setup({
                options = {
                    theme = custom_theme
                },
                sections = {
                    lualine_c = {
                        {
                            'filename',
                            path = 3,
                        },
                    },
                },
                -- sections to show for buffers other than the current one
                inactive_sections = {
                    lualine_c = {
                        {
                            'filename',
                            path = 3,
                        },
                    }
                }
            })
        end,
    },
}

-- plugins that provide significant IDE functionality but do not depend on language servers
basic_ide_plugins = {
    {
        -- interface for different fuzzy finding searching methods
        'nvim-telescope/telescope.nvim',
        tag = '0.1.5',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'nvim-tree/nvim-web-devicons',
        },
    },
    {
        -- highlight other uses of the word under the cursor. Can use treesitter or lsp
        'RRethy/vim-illuminate',
    },
    {
        -- autocomplete from various sources
        'hrsh7th/nvim-cmp',
        lazy = true,
        event = 'InsertEnter',
        dependencies = {
            'hrsh7th/cmp-nvim-lsp',  -- lsp client autocomplete source
            'hrsh7th/cmp-buffer', -- current buffer words autocomplete source
            --'hrsh7th/cmp-path', -- filesystem paths autocomplete source
            'hrsh7th/cmp-nvim-lsp-signature-help' -- provides hints when typing function call
        },
        config = function()
            local cmp = require('cmp')
            cmp.setup({
                snippet = {
                    expand = function(args) end,
                },
                mapping = cmp.mapping.preset.insert({
                    ['<C-Space>'] = cmp.mapping.complete(), -- manually open autocomplete
                    ['<C-n>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
                    ['<C-p>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
                    ['<Tab>'] = cmp.mapping.confirm({
                        -- select = false => explicitly selected only
                        select = true
                    }),
                }),
                sources = cmp.config.sources(
                    -- grouped by priority
                    {
                        { name = 'nvim_lsp' },
                        { name = 'nvim_lsp_signature_help' },
                    },
                    {
                        { name = 'buffer' },
                        { name = 'path' },
                    }
                )
            })
        end,
    },
    {
        -- provides syntax highlighting and some language specific text objects. Can handle nested langauges (eg code
        -- blocks inside markdown)
        -- lsp does not fully replace because not all languages have lsp that implements syntax highlighting
        -- and lsp may be slower to provide it.
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function ()
          local configs = require("nvim-treesitter.configs")
          configs.setup({
              -- supported languages listed here: https://github.com/nvim-treesitter/nvim-treesitter
              ensure_installed = {
                  "bash",
                  "c",
                  "cmake",
                  "cpp",
                  "comment",  -- highlight todo in buffers using treesitter
                  "css",
                  "diff",
                  "dockerfile",
                  "dot",
                  "gdscript",
                  "haskell",
                  "html",
                  "java",
                  "javascript",
                  "json",
                  "json5",
                  "latex",
                  "lua",
                  "make",
                  "markdown",
                  "markdown_inline",
                  "nasm",
                  "ocaml",
                  "python",
                  "query",  -- treesitter query language
                  "rst",
                  "rust",
                  "sql",
                  "toml",
                  "typescript",
                  "vim",
                  "vimdoc",
                  "wgsl",
                  "xml",
                  "yaml",
              },
              sync_install = false,
              highlight = { enable = true },
              indent = { enable = true },
            })
        end
    },
    {
        -- justfile
        'NoahTheDuke/vim-just',
        lazy = true,
        ft = 'just',
    },
}

-- plugins that depends on language servers being present on the system
language_server_plugins = {
    {
        -- neovim has an lsp client built-in but does not contain any configuration defaults. This plugin provides
        -- sensible default configurations for popular language servers
        "neovim/nvim-lspconfig",
    },
    {
        -- show lsp errors in a pane at the bottom
        "folke/trouble.nvim",
        lazy = true,
        dependencies = { "nvim-tree/nvim-web-devicons" },
        cmd = { 'Trouble', 'TroubleToggle', 'TroubleRefresh' },
    },
    {
        -- provide breadcrumbs showing the current class/method etc if the language server supports it.
        -- does not automatically display the breadcrumbs. Use ToggleBreadcrumbs to show or hide.
        "SmiteshP/nvim-navic",
    },
    {
        -- list symbols in the current buffer
        'simrat39/symbols-outline.nvim',
        lazy = true,
        cmd = { 'SymbolsOutline' },
        config = function()
            require('symbols-outline').setup{}
        end
    },
    {
        -- show code actions in telescope instead of as a built-in selection menu at the bottom of the screen
        "aznhe21/actions-preview.nvim",
    },
    {
        -- run auto-formatters with fallback to lsp
        "stevearc/conform.nvim",
        event = { "BufWritePre" },
        cmd = { "ConformInfo" },
        opts = {
            formatters_by_ft = {
                python = { "ruff_fix", "ruff_format" },
                -- rust analyzer lsp can handle rust formatting
            },
            formatters = {
                ruff_fix = {
                    -- I: isort
                    prepend_args = { "--select", "I" }
                }
            }
        }
    },
    {
        -- manage Cargo.toml with vim
        'saecki/crates.nvim',
        tag = 'stable',
        lazy = true,
        event = { "BufRead Cargo.toml" },
        dependencies = { 'nvim-lua/plenary.nvim' },
        config = function()
            require('crates').setup({})
        end,
    },
}

