-- utility functions to be used to create user-facing functionality

local M = {}

-- escapes termcodes like to the internal representation (e.g. '<C-w>' -> '^W')
function M.replace_termcodes(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end


local Set = {}

function Set:new(elements)
    local set = {}
    setmetatable(set, self)
    self.__index = self -- to allow for Set.<method> style calling as well as <object>:<method>
    if elements then
        set:update(elements)
    end
    return set
end

function Set:add(element)
    self[element] = true
end

function Set:remove(element)
    self[element] = nil
end

function Set:update(elements)
    for _, val in ipairs(elements) do
        self[val] = true
    end
    return self
end

function Set:contains(value)
    return self[value] ~= nil
end

M.Set = Set

return M
