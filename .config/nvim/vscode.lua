------------------------------------------------
-- Configuration for the vscode-neovim plugin --
------------------------------------------------

-- note: keybindings involving modifier keys need special configuration to be passed to neovim
-- so in some cases it may be better to configure those in keybindings.json
-- https://github.com/vscode-neovim/vscode-neovim?tab=readme-ov-file#add-keybindings
--
-- note: use vscode.notify() for an easy way to send debug messages


-- TODO(matt): add keybinding for 'go to next spelling mistake and suggest'
-- TODO(matt): improve jumplist by making C-j and C-k at least not add entries to the jumplist
-- TODO(matt): make autocomplete not trigger automatic imports in python... it is annoying and rarely useful

-- bootstrap lazy vim
-- https://github.com/folke/lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
-- bootstrap end


require("lazy").setup({
    {
        -- lua standard library
        'nvim-lua/plenary.nvim'
    },
    {
        -- keybindings for surrounding selection with a character
        "tpope/vim-surround",
        branch = "master"
    },
    {
        -- use `.` to repeat plugin actions. (for vim-surround and others)
        "tpope/vim-repeat",
        branch = "master"
    },
    {
        -- search for visual selected text with * and # (useful for Unicode text)
        "bronson/vim-visual-star-search",
    },
    {
        -- align by selecting a range and using `Tab`. Eg to align equals signs: :'<,'>Tab /=
        "godlygeek/tabular",
        lazy = true,
        cmd = { "Tabularize" },
    },
    {
        -- run auto-formatters with fallback to lsp
        "stevearc/conform.nvim",
        event = { "BufWritePre" },
        cmd = { "ConformInfo" },
        opts = {
            formatters_by_ft = {
                python = { "ruff_fix", "ruff_format" },
            },
            formatters = {
                ruff_fix = {
                    -- I: isort
                    prepend_args = { "--select", "I" }
                }
            }
        }
    },
})

require("user.functions")      -- ./lua/user/functions.lua

local vscode = require('vscode')


vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.g.mapleader = ','
vim.opt.clipboard = 'unnamedplus'  -- use system clipboard for everything

vim.keymap.set({'n', 'v'}, '<C-j>', '10j')
vim.keymap.set({'n', 'v'}, '<C-k>', '10k')

-- change indent without leaving visual mode
vim.keymap.set('v', '<', '<gv', { noremap = true, silent = true })
vim.keymap.set('v', '>', '>gv', { noremap = true, silent = true })

vim.keymap.set('n', '<space>', function() vscode.action('editor.action.revealDefinition') end)
vim.keymap.set('n', '+', function() vscode.action('editor.action.fontZoomIn') end)
vim.keymap.set('n', '-', function() vscode.action('editor.action.fontZoomOut') end)

vim.keymap.set('n', '<leader>/',  function() vscode.action('workbench.action.findInFiles') end)
vim.keymap.set('n', '<leader>b',  function() vscode.action('editor.debug.action.toggleBreakpoint') end)
vim.keymap.set('n', '<leader>m',  function() vscode.action('bookmarks.toggleLabeled') end)
vim.keymap.set('n', '<leader>\'',  function() vscode.action('bookmarks.listFromAllFiles') end)
vim.keymap.set('n', '<leader>gf', function() vscode.action('workbench.action.quickOpen') end)
vim.keymap.set('n', '<leader>gs', function() vscode.action('workbench.action.showAllSymbols') end)
vim.keymap.set('n', '<leader>gr', function() vscode.action('editor.action.referenceSearch.trigger') end)
vim.keymap.set('n', '<leader>oi', function() vscode.action('editor.action.organizeImports') end)
vim.keymap.set('n', '<leader>sw', function() vscode.action('editor.action.trimTrailingWhitespace') end)
vim.keymap.set('n', '<leader>t',  function() vscode.action('editor.action.formatDocument') end)
vim.keymap.set('n', '<leader>va', function() vscode.action('gitlens.toggleFileBlame') end)
vim.keymap.set('n', '<leader>zg', function() vscode.action('cSpell.addWordToUserDictionary') end)
vim.keymap.set('n', '<leader>q', function() vscode.action('editor.action.quickFix') end)
vim.keymap.set('n', '<leader><CR>', 'o# %%<ESC>o')
vim.keymap.set('n', '<leader>1', function()
    vscode.eval_async([[
        if (vscode.window.tabGroups.activeTabGroup.activeTab.isPinned) {
            await vscode.commands.executeCommand('workbench.action.unpinEditor');
        } else {
            await vscode.commands.executeCommand('workbench.action.pinEditor');
        }
    ]])
end)
vim.keymap.set('n', '<leader>\\', function()
    vscode.eval_async(
        [[
            return await new Promise((resolve) => {
                const box = vscode.window.createInputBox();
                box.title = "Digraph";
                box.onDidChangeValue((event) => {
                    if (box.value.length >= 2) { resolve(box.value); box.dispose(); }
                });
                box.onDidAccept((event) => { resolve(box.value); box.dispose(); });
                box.onDidHide((event) => { resolve(null); box.dispose(); });
                box.show();
            });
        ]],
        {
            callback = function(err, response)
                if response ~= vim.NIL and response:len() == 2 then
                    vim.fn.execute('normal i' .. vim.fn.digraph_get(response))
                end
            end
        }
    )
end)

vim.keymap.set('n', '<leader>tt', InsertTODO)

vim.keymap.set('n', '<leader>f', function()
    -- cannot use lsp fallback here as the lsp is running in vscode instead of nvim
    if vim.bo.filetype == "python" then
        require('conform').format({ async = true, lsp_fallback = false })
    else
        vscode.action('editor.action.formatDocument')
    end
end)

-- by default, gq is mapped to vscode formatting
vim.opt.textwidth = 120
vim.keymap.del({'n', 'v'}, 'gq')

vim.api.nvim_create_user_command('Theme', function(opts)
    vscode.action('workbench.action.selectTheme')
end, {})

vim.api.nvim_create_user_command('OpenSettings', function(opts)
    vscode.action('workbench.action.openSettingsJson')
end, {})

vim.api.nvim_create_user_command('OpenKeyboardShortcuts', function(opts)
    vscode.action('workbench.action.openGlobalKeybindings')
end, {})

function open_file(path)
    -- see vscode-neovim/runtime/overrides to see how :e is handled
    vscode.eval_async(
        [[
            let doc = await vscode.workspace.openTextDocument(vscode.Uri.file(args[0]));
            await vscode.window.showTextDocument(doc, { preview: false });
        ]],
        { args = {path} }
    )
end

function current_path()
    return vscode.eval('return vscode.window.activeTextEditor.document.fileName')
end

vim.api.nvim_create_user_command('OpenNeovimConfig', function(opts)
    open_file(vim.fn.stdpath('config') .. '/vscode.lua')
end, {})

function LeptosFmt()
    local path = current_path()
    if path ~= nil then
        print("running leptosfmt on " .. path)
        vim.fn.system({'leptosfmt', path})
    end
end
vim.api.nvim_create_user_command('LeptosFmt', function(opts) LeptosFmt() end, {})
vim.keymap.set('n', '<leader>lf', LeptosFmt)

vim.api.nvim_create_user_command('JsonFmt',
    function(opts) JsonFmt(opts.args, opts.range) end,
    { nargs = '?', range = true }
)
