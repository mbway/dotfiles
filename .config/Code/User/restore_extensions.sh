#!/usr/bin/env bash

if command -v vscodium &> /dev/null; then
    CODE=vscodium
elif command -v codium &> /dev/null; then
    CODE=codium
else
    CODE=code
fi

cat extensions.txt | xargs -n 1 ${CODE} --install-extension
