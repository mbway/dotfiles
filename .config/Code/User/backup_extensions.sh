#!/usr/bin/env bash

# the list of extensions can also be found in json form in `~/.vscode-oss/extensions/extensions.json`
# extension data is also stored to `~/.vscode-oss/extensions` and removing this directory uninstalls all extensions

if command -v vscodium &> /dev/null; then
    CODE=vscodium
else
    CODE=code
fi

${CODE} --list-extensions | sort > extensions.txt
