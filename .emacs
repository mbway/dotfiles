;; Notes
;; use #|| and ||# for multi-line comments
;;  M-x package-list-package to list and install packages
;;  M-x package-install <RET> some-package <RET> to install a new package
;;  M-x flyspell-mode to activate spelling


;; load build-in package manager
(require 'package)
(setq package-archives '(("gnu"       . "https://elpa.gnu.org/packages/")
                         ;("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa"     . "https://melpa.org/packages/")))
(package-initialize)

;; add org mode package archive
;(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)


;; list packages in the config rather than manually installing
(unless (package-installed-p 'use-package)
 (package-refresh-contents)
 (package-install 'use-package))
(eval-when-compile
 (require 'use-package))


;; Installed Packages
; Vim emulation
(use-package evil :ensure t)
; easily bind keys with a specified prefix key
(use-package evil-leader :ensure t)
; line numbers
(use-package nlinum :ensure t)
; org mode
;(use-package org :ensure t)



;; Misc settings
(setq inhibit-startup-screen 1)
(setq frame-title-format "emacs") ; as opposed to emacs@machine
(tool-bar-mode -1)   ; disable icon bar
(global-nlinum-mode) ; line numbers
(column-number-mode) ; shows cursor position in the mode line
;(scroll-bar-mode -1) ; disable scrollbar
(add-hook 'text-mode-hook 'flyspell-mode)       ; spell check
(add-hook 'prog-mode-hook 'flyspell-prog-mode)  ; spell check
(setq show-paren-delay 0)
    (show-paren-mode); hi-light matching parentheses
(setq-default tab-width 4 indent-tabs-mode nil) ; tabs settings
(setq-default fill-column 120) ; word wrap
; temporary-file-directory == /tmp/
(setq backup-directory-alist
	  `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
	  `((".*" ,temporary-file-directory t)))
; Smooth Scrolling
(setq mouse-wheel-scroll-amount '(2 ((shift) . 1))) ; 2 lines at a time
(setq mouse-wheel-progressive-speed nil)            ; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't)                  ; scroll window under mouse



;; Style Settings
;(load-theme 'wombat)
;(load-theme 'tango-dark)
(load-theme 'monokai t)  ; package: 'monokai-theme' needs to be installed

;(set-frame-font "Monospace 14") ; default (default size 11)
;(set-frame-font "Monaco 16")
;(set-frame-font "Inconsolata Medium 14")
(set-frame-font "DejaVu Sans Mono 15")
;(set-frame-font "Droid Sans Mono 15")
;(set-frame-font "Hack 15")
;(set-frame-font "TeX Gyre Chorus 16")
;(set-frame-font "Comic Sans MS 14")

;; fallback font (for Unicode characters which are not found in the default font)
; (nil as the glyph range => fallback)
; DejaVu Sans contains more glyphs than the mono version
(set-fontset-font "fontset-default" nil (font-spec :name "DejaVu Sans"))



;; Custom Bindings
(global-set-key (kbd "<f5>") 'recompile) ; run make (using last parameters (set by calling M-x compile))



;; evil-mode
;; \ to enter emacs mode for a single command (eg to disable)
(require 'evil)
(evil-mode 1)   ; evil-mode at startup
(global-set-key (kbd "M-e") 'evil-mode) ; toggle evil mode with M-e

(require 'evil-leader)
(global-evil-leader-mode) ; enable evil-leader wherever evil mode is enabled

(evil-leader/set-leader "\\")

; easier access to the emacs kill ring (to paste from outside)
(evil-leader/set-key
 "y" 'kill-ring-save
 "p" 'yank           ; (yank = paste)
 "l" 'agda2-load
 "<SPC>" 'agda2-give)


; use * and # in visual mode to search using the selection
(require 'evil-visualstar)
(global-evil-visualstar-mode t)



;; annoyances/problems with evil-mode (maybe one day fix?)
; - spell checking commands, zg z= etc
; - paragraph movement is broken eg doesn't always move to the line above the
;   first line of the paragraph and the following when in latex mode (without ; for comments)
; \item abc
;       def
; \item abc
; - gq doesn't add comments on the new lines where necessary



;; agda2-mode
(load-file (let ((coding-system-for-read 'utf-8))
   (shell-command-to-string "agda-mode locate")))
; fix colors for dark theme (different from default when used with default light theme)
; from https://lists.chalmers.se/pipermail/agda/2012/004597.html
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(agda2-highlight-datatype-face ((t (:inherit font-lock-type-face))))
 '(agda2-highlight-field-face ((t (:foreground "#ad7fa8"))))
 '(agda2-highlight-function-face ((t (:inherit font-lock-function-name-face))))
 '(agda2-highlight-inductive-constructor-face ((t (:foreground "#8ae234"))))
 '(agda2-highlight-keyword-face ((t (:inherit font-lock-keyword-face))))
 '(agda2-highlight-module-face ((t (:inherit font-lock-builtin-face))))
 '(agda2-highlight-number-face ((t (:inherit font-lock-constant-face))))
 '(agda2-highlight-postulate-face ((t (:inherit font-lock-type-face))))
 '(agda2-highlight-primitive-face ((t (:inherit font-lock-type-face))))
 '(agda2-highlight-primitive-type-face ((t (:inherit font-lock-type-face))))
 '(agda2-highlight-record-face ((t (:inherit font-lock-type-face))))
 '(agda2-highlight-string-face ((t (:inherit font-lock-string-face)))))

(setq agda-input-user-translations `(
                                     ("lm" . ("λ")) ; \lambda
                                     ("sm" . ("☻")) ; \blacksmiley
                                     ("n"  . ("ℕ")) ; \bN
                                     ("er" . ("? ≡⟨ ? ⟩ ? ∎")) ; equational reasoning
                                     ("fa" . ("∀")) ; \forall
                                     ))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (use-package nlinum monokai-theme evil-visualstar evil-visual-mark-mode evil-leader))))
