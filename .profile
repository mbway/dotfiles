# sourced during both graphical and tty logins
# KDE doesn't source this so another file: ~/.config/plasma-workspace/env/profile.sh should be created to source this file

# a hack to avoid having to keep typing in the gpg password
"$HOME/.gnupg/load_passphrase"

. "$HOME/.setenv"
if [ -f "$HOME/.cargo/env" ]; then
    . "$HOME/.cargo/env"
fi
