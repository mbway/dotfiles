Dotfiles
========

# Manual Installation Steps #
- add the line `. "$HOME/.setenv"` to `~/.profile`
- run `./unpack`
- when `vim` is first used, `Lazy` will install plugins
- run `systemctl --user enable tmp_workspace kanata`
