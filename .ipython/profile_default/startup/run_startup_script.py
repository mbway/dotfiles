import os
startup_script_path = os.environ.get('JUPYTER_STARTUP_SCRIPT')
if startup_script_path is not None:
    import sys
    module_path = os.path.dirname(startup_script_path)
    sys.path.insert(0, module_path)
    module_name = os.path.splitext(os.path.basename(startup_script_path))[0]
    import importlib.machinery
    startup_script = importlib.machinery.SourceFileLoader(module_name, startup_script_path).load_module()
    globals().update({k: v for k, v in startup_script.__dict__.items() if not k.startswith('_')})
